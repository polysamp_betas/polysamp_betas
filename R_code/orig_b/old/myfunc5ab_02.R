shrink_Prep <- function(Q12in, yin) {
    m <- nrow(Q12in)
    d <- ncol(Q12in)
    n <- m + d
    perm <- 1:n
    idel <- NULL
    Ain <- cbind(diag(m), - Q12in)
    for (j in 1:n) {
        objfun <- rep(0,n)
        objfun[j] <- 1
        lpmin <- lp(direction="min", objective.in=objfun, const.mat=Ain, const.dir=rep("=",m), const.rhs=yin)
        if (lpmin$objval > 0) {
            idel <- c(idel, j)
        }
    }
    if (is.null(idel)) {
        return(list(Q12=Q12in, y=yin, preperm=perm, rowdel=NULL, ydel=NULL, idel=NULL))
    }
    idx2 <- which(idel > m)
    if (length(idx2 > 0)) {
        idel2 <- idel[idx2]
        idx1 <- which(idel <= m)
        if (length(idx1 > 0)) {
            idel1 <- idel[idx1]
            kdx <- setdiff(1:m, idel1)
        } else {
            idel1 <- NULL
            kdx <- 1:m
        }
        success <- 0
        while (success == 0) {
            perm <- 1:n
            idx <- sample(kdx, length(idel2))
            perm[idx] <- idel2
            perm[idel2] <- idx
            A <- Ain[,perm]
            A1 <- A[,1:m, drop=F]
            if (det(A1) != 0) success <- 1
        }
        idel <- sort(c(idel1, idx))
        A2 <- A[,(m+1):n,drop=F]
        Q12 <- -solve(A1,A2)
        y <- solve(A1,yin)
    } else {
        Q12 <- Q12in
        y <- yin
    }
    rowdel <- Q12[idel,,drop=F]
    Q12 <- Q12[-idel,,drop=F]
    ydel <- y[idel]
    y <- y[-idel]
    return(list(Q12=Q12, y=y, preperm=perm, rowdel=rowdel, ydel=ydel, idel=idel))
}

add_miss <- function(x, xmiss, idel) {
    sp <- sort.int(idel, index.return=T)
    xmiss <- xmiss[sp$ix]
    idxdel <- sp$x
    for (i in 1:length(idel)) {
        ii <- idel[i]
        x <- c(x[myseq(1,ii-1)], xmiss[i], x[myseq(ii,length(x))])
    }
    return(x)
}

getback_x <- function(x, Q12, y, preperm, rowdel, ydel, idel) {
    m <- nrow(Q12)
    d <- ncol(Q12)
    n <- m + d
    if (is.null(idel)) return(x)
    xb <- x[(m+1):n]
    xmiss <- ydel + rowdel %*% xb
    xx <- add_miss(x, xmiss, idel)
    xx[preperm] <- xx
    return(xx)
}

b_sampx <- function(Q12in, yin, psin) {
    if (is.null(Q12in) | ncol(Q12in) == 0) {
        return(yin)
    }
    if (all(yin == 0)) return(rep(0,n))
    sh <- shrink_Prep(Q12in, yin)
    shQ12 <- sh$Q12; shy <- sh$y; shperm <- sh$preperm
    shrowdel <- sh$rowdel; shydel <- sh$ydel; shidel <- sh$idel
    m <- nrow(shQ12)
    d <- ncol(shQ12)
    n <- m + d
    lpmax <- lp(direction="max", objective.in=c(rep(0,d-1),1), const.mat=-shQ12, const.dir=rep("<=",m), const.rhs=shy)
    xd <- psin[d] * lpmax$objval
    ynext <- shy + xd * shQ12[,d]
    Q12next <- shQ12[,-d,drop=F]
    psnext <- psin[-d]
    x1 <- b_sampx(Q12next, ynext, psnext)
    x2 <- c(x1,xd)
    x3 <- getback_x(x2, shQ12, shy, shperm, shrowdel, shydel, shidel)
    if (any(x3 <= 0)) {
        cat("x3 non postive\n")
        browser()
    }
    return(x3)
}

AtoQ12 <- function(Ain,bin) {
    m <- nrow(Ain)
    n <- ncol(Ain)
    d <- n - m
    ikeep <- NULL
    rowdel <- NULL
    bdel <- NULL
    idel <- NULL
    r <- qr(Ain)$rank
    if (r < m) {
        ikeep <- qr(t(Ain))$pivot[1:r,drop=F]
        idel <- setdiff(1:m, ikeep)
        rowdel <- Ain[idel,,drop=F]
        bdel <- bin[idel]
        A <- Ain[ikeep,,drop=F]
        m <- r
        d <- n-m
        b <- bin[ikeep]
    } else {
        A <- Ain
        b <- bin
    }
    if (d == 0 | r == 0) {
        return(list(Q12=NULL, y=bin, colperm=NULL, rowperm=ikeep, rowdel=rowdel, bdel=bdel, idxdel=idel, rank=r))
    }
    colperm <- qr(A)$pivot
    A <- A[,colperm,drop=F]
    A1 <- A[,1:m,drop=F]
    A2 <- A[,myseq(m+1,n),drop=F]
    Q12 <- -solve(A1,A2)
    y <- solve(A1,b)
    return(list(Q12=Q12, y=y, colperm=colperm, rowperm=ikeep, rowdel=rowdel, bdel = bdel, idxdel=idel, rank=r))
}

tround <- function(x) return(TOL*round(x/TOL))

#A is already supposed to have full rank
bb_sampx <- function(A, b, nsamp, alf, bet) {
    n <- ncol(A)
    m <- nrow(A)
    d <- n-m
    presamp <- matrix(0, nrow=nsamp, ncol=d)
    alfstep <- (alf-1)/(d-1)
    for (j in 1:d) {
        salf <- alf - (j-1) * alfstep
        sbet <- salf
        presamp[,j] <- rbeta(nsamp, salf, sbet)
    }
    Xsamp <- matrix(0,ncol=n, nrow=nsamp)
    for (k in 1:nsamp) {
        perm1 <- sample(n,n)
        AQ <- AtoQ12(A[,perm1], b)
        Q12 <- AQ$Q12; y <- AQ$y; perm2 <- perm1[AQ$colperm]
        if (!is.null(AQ$rowdel)) {
            cat("Rwos have been deleted by AtoQ12\n")
            browser()
        }
        x <- b_sampx(Q12, y, presamp[k,])
        Xsamp[k,perm2] <- x
    }
    return(Xsamp)
}


testab_bb_sampx <- function(A, b, nsamp){
    n <- ncol(A)
    m <- nrow(A)
    d <- n - m
    dseq <- seq(1,(d+1)/2,by=0.5)
    nab <- length(dseq)
    abXsamp <- array(0,dim=c(nsamp, n, nab))
    for (i in 1:nab) {
        abXsamp[,,i] <- bb_sampx(A, b, nsamp, dseq[i], dseq[i])
    }
    return(abXsamp)
}

refine_trig <- function(Vert, slist, svol, thresh) {
    d <- ncol(slist) - 1
    nsimp <- nrow(slist)
    nnodes <- nrow(Vert)
    i <- 1
    while (i <= nsimp) {
        if (svol[i] > thresh) {
            #cat(i, " out of", nsimp, " diff = ", nsimp - i, "\n")
            simp <- slist[i,]
            newnode <- colSums(Vert[simp,])/(d+1)
            Vert <- rbind(Vert, newnode)
            nnodes <- nnodes + 1
            svol <- svol[-i]
            slist <- slist[-i,]
            for (j in 1:(d+1)) {
                newsimp <- c(nnodes, simp[-j])
                newvol <- get_volsimp(Vert, newsimp)
                svol <- c(svol, newvol)
                slist <- rbind(slist, newsimp)
            }
            nsimp <- nsimp + d
        } else {
            i <- i+1
        }
    }
    return(list(Vert=Vert, slist=slist, svol=svol))
}

get_volsimp <- function(Vert, simp) {
    n <- ncol(Vert)
    d <- length(simp) - 1
    V <- Vert[simp,]
    S <- t(V[1:d,]) - V[d+1,]
    vol <- sqrt(abs(det(t(S) %*% S)))
    return(vol)
}

get_scount <- function(Xsamp, Vert, simp) {
    nsamp <- nrow(Xsamp)
    d <- length(simp) - 1
    Xcount <- rep(0, nsamp)
    V <- Vert[simp,]
    qrV <- qr(V)
    projdim <- qrV$pivot[1:(d+1)]
    Vproj <- V[,projdim]
    Xproj <- solve(t(Vproj), t(Xsamp[,projdim,drop=F]))
    check_pos <- function(x) {
        if (all(x >= 0)) return(1)
        return(0)
    }
    Xcount <- apply(Xproj, 2, check_pos)
    count <- sum(Xcount)
    idx <- which(Xcount == 0)
    Xsamp <- Xsamp[idx,]
    return(list(Xsamp=Xsamp, count=count))
}

get_scount_bigq <- function(Xsamp, Vert, simp) {
    nsamp <- nrow(Xsamp)
    d <- length(simp) - 1
    Xcount <- rep(0, nsamp)
    V <- Vert[simp,]
    qrV <- qr(V)
    projdim <- qrV$pivot[1:(d+1)]
    Vproj <- V[,projdim]
    Xprojq <- as.bigq(solve(t(Vproj), t(Xsamp[,projdim])))
    Xsign <- matrix(sign(Xprojq), nrow=nrow(Xprojq))
    check_pos <- function(x) {
        if (all(x >= 0)) return(1)
        return(0)
    }
    Xcount <- apply(Xsign, 2, check_pos)
    count <- sum(Xcount)
    idx <- which(Xcount == 0)
    Xsamp <- Xsamp[idx,]
    return(list(Xsamp=Xsamp, count=count))
}

get_scountvec <- function(Xsamp, Vert, simplist) {
    nsimp <- nrow(simplist)
    scountv <- rep(0,nsimp)
    for (i in 1:nsimp) {
        gs <- get_scount(Xsamp, Vert, simplist[i,])
        scountv[i] <- gs$count
        Xsamp <- gs$Xsamp
    }
    return(scountv)
}

get_gscountv <- function(scountv, glentest) {
    ntest <- length(glentest)
    gscountv <- rep(0, ntest)
    last = 0
    for (i in 1:ntest) {
        first <- last + 1
        last <- last + glentest[i]
        gscountv[i] <- sum(scountv[first:last])
    }
    return(gscountv)
}


