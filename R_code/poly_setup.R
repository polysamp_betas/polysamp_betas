###################################################
#  Setting up the system main variables
###################################################

.ROOTPATH <- "~/Google Drive/research/polysamp_betas/R_code/"

.path.struct <- paste(.ROOTPATH, "data_poly_struct/", sep ="")
.path.stat <- paste(.ROOTPATH, "data_poly_stat/", sep="")
.path.samps <- paste(.ROOTPATH, "data_poly_samps/tXsamp_A0/", sep="")

A_0 <- as.matrix(read.table(paste(.path.struct, "A_0.txt", sep="")))
A_1 <- as.matrix(read.table(paste(.path.struct, "A_1.txt", sep="")))
permut01 <- c(as.matrix(read.table(paste(.path.struct, "permut01.txt", sep=""))))

b <- c(as.matrix(read.table(paste(.path.struct, "b.txt", sep=""))))
n <- ncol(A_0)
m <- nrow(A_0)
d <- n-m

#A1in <- A_1[,1:m]
#A2in <- A_1[,(m+1):n]
#Q12in <- -solve(A1in, A2in)
#yin <- solve(A1in, b)
#permid <- 1:n

Xxrange <- as.matrix(read.table(paste(.path.stat, "Xxrange.txt", sep="")))
Xymax <- as.matrix(read.table(paste(.path.stat, "Xymax.txt", sep="")))

Vert <- as.matrix(read.table(paste(.path.struct, "Vert.txt", sep="")))
nvert <- nrow(Vert)
distA <- as.numeric(read.table(paste(.path.struct, "distA.txt", sep="")))

source(paste(.ROOTPATH, "utils.R", sep=""))
source(paste(.ROOTPATH, "poly_utils.R", sep=""))
source(paste(.ROOTPATH, "poly_struct.R", sep=""))
source(paste(.ROOTPATH, "poly_sample.R", sep=""))
source(paste(.ROOTPATH, "poly_stat.R", sep=""))
TOL <<- 1e-14

