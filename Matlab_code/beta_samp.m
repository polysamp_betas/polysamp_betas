function x = beta_samp(A,b)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
global TOL
[m n] = size(A);
d = n-m;
if (d == 0)
    x = round(A\b);
    if (any(x < 0))
        disp 'x not non-negative, system infeasible';
    end
    return
end
i = floor(n*rand) + 1;
f = zeros(1,n);
f(i) = 1;
t = plp(0,f,A,b);
t0 = t(i);
t = plp(1,f,A,b);
tN = t(i);
if (tN == t0)
    xi = t0;
else
    ac = get_acent(A,b);
    alf = (d + 1) * (ac(i) - t0)/(tN - t0);
    bet = d + 1 - alf;
    xi = t0 + round((tN - t0) * betarnd(alf,bet));
end
bb = b - xi * A(:,i);
AA = A;
AA(:,i) = [];
if (rank(AA) < m)
    NAA = truncTOL(null(AA'),TOL);
    rowrem = [];
    for k = 1:size(NAA,2)
        if (all(NAA(:,k) == 0))
            continue;
        end
        idx1 = setdiff(find(NAA(:,k) ~= 0), rowrem);
        [~, imin] = min(abs(NAA(idx1,k)));
        rowrem = [rowrem, idx1(imin)];
    end
    AA(rowrem,:) = [];
    bb(rowrem) = [];
end
x = beta_samp(AA,bb);
x = [x(1:i-1); xi; x(i:n-1)];
end



