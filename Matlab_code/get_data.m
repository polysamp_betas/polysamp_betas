A_1 = importdata('A_1.txt');
A_0 = importdata('A_0.txt');
m = size(A_1,1);
n = size(A_1,2);
d = n-m;
b = importdata('b.txt');
Vert = importdata('Vert.txt');
nvert = size(Vert,1);
permut01 = importdata('permut01.txt')';
global TOL
TOL = 1e-14;