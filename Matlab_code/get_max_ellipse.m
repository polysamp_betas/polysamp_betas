A1 = A_1(:,1:m);
A2 = A1\A_1(:,(m+1):n);
b2 = A1\b;
Q2 = [A2; -eye(d)];
y = [b2; zeros(d,1)];
bcent = sum(Vert)/nvert;

cvx_begin quiet
    cvx_precision best
    variable B(d,d) symmetric;
    B == semidefinite(d);
    variable ch(d);
    maximize( det_root2n(B) )
    subject to
       for i = 1:m
           norm( B*Q2(i,:)', 2 ) + Q2(i,:)*ch <= y(i)/1000000;
       end
cvx_end