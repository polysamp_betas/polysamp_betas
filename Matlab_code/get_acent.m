
function x=get_acent(A,b)
n = size(A,2);
cvx_begin quiet
    cvx_precision best;
    variable x(n);
    minimize( -sum(log(x)) );
    subject to
       A * x == b;
cvx_end
end
