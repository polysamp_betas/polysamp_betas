
function x=get_acent2(A,b)
n = size(A,2);
cvx_begin quiet
    cvx_precision best;
    variable x(n);
    minimize( -sum(log(b - A*x)) );
cvx_end
end
