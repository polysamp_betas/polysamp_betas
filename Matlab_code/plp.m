function x = plp(minmax, f, A, b)
%plp(m,f,A,b) for Positive Linear Programming
%   maximize (m=1) or minimize (m=0) f * x over polyhedron Ax = b, x >= 0
n = size(A,2);
cvx_begin quiet
    cvx_precision best;
    variable x(n);
    if (minmax == 0)
        minimize(f*x);
    else
        maximize(f*x);
    end
    subject to
        A * x == b;
        x >= 0;
cvx_end
end

