function y = truncTOL(x, TOL)
%truncTOL(x) returns the value of x truncated to TOL
y = round(x/TOL)*TOL;
end
