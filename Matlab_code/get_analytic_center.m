function x=get_acent(A_0,b)
%Gets the analytical center of a polyhedron A_0*x=b, x >=0
n = size(A_0,2);
cvx_begin
    cvx_precision best
    variable x(n);
    minimize( -sum(log(x)) )
    subject to
       A_0 * x == b
cvx_end