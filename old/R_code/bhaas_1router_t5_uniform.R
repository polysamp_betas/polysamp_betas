chainLength <- 100
chainNum <- 00
rateVerif <- 50/100

#time of interest (integer > 0)
toi <- 5

##########(1)  THE DATA
source("bhaas_1rout_data.R")

#extract point at time of interest and y (Kbytes/sec.)
tp <- r1link[(nlk*(toi-1)+1):(nlk*toi),]
y <- round(300*tp$value[1:nlk-1])

#extract message counts at time of interest (Kbytes/sec)
tp <- r1count[(nsd*(toi-1)+1):(nsd*toi),]
mcount <- round(300*tp$value)

#########(2)  THE ALGORITHM
source("bhaas_myfunctions.R")

rootname <- "bhaas_chain_uninf_"

fname <- paste(rootname,formatC(chainNum,width=3,flag="0"),".txt",sep="")
file.create(fname)

samplesol(y,A,ns=chainLength,fname=fname)

#random verif
sol <- as.matrix(read.table(fname))
nverif <- round(rateVerif * nrow(sol))
idx <- sample.int(nrow(sol),nverif)
cat("discrepancy = max|A*sol[i] - y|:\n")
for (i in idx) {
    d <- dist1((A %*% sol[i,] - y))
    cat("d[",i,"] = ",d,"\n")
}

d1 <- 0; d2 <- 0
for (i in idx) {
    d2 <- d2 + dist2(sol[i,]-mcount)/nverif
    d1 <- d1 + dist1(sol[i,]-mcount)/nverif
}
cat("average estimate of \"good-guessing\":\n")
cat("d1 = ",d1," d2 = ",d2,"\n")
