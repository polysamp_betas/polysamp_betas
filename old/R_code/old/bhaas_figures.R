rout1 <- read.table("1router_allcount.dat",header=T,sep=",")
times <- strptime(rout1$time, "(%m/%d/%y %H:%M:%S)")
rout1$hour <- times$hour + times$min/60 + times$sec/3600
rout1$link_grp <- "no"
rout1$link_grp[grep("src", rout1$nme)] <- "src"
rout1$link_grp[grep("dst", rout1$nme)] <- "dst"
rout1$link_OD <- "no"
idx <- grep("src|dst",rout1$nme)
rout1$link_OD[idx] <- gsub("src |dst ","",rout1$nme[idx])

pdf("bhaas_figure2.pdf")
xyplot(value~hour|link_OD,
        data=rout1[idx,],
        groups=link_grp,
        layout=c(1,4),
        index.cond=list(c(2,4,3,1)),
        type="l",
        aspect=1/8,
        scales=list(
            x=list(at=c(0,4,8,12,16,20,24)),
            y=list(at=c(0,200000,400000,600000,800000,1000000),
            labels=c("0","200K","400K","600K","800K","1M"))),
            xlab="hour of day",
            ylab="bytes/sec",
            auto.key=list(
                columns=2,
                lines=T,
                points=F,
                text=c("origin","destination")
            )
        )
dev.off()
