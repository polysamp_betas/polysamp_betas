hnf <- function(A) {
    nr <- nrow(A)
    nc <- ncol(A)
    Q <- diag(nc)
    B <- A
    for (i in 1:nr) {
        j <- i
        while (j <= ncol(B) && B[i,j] == 0) { j <- j+1 }
        if (j > ncol(B)) {
            warning("looks like A has not full row-rank")
            break
        }
        if (j > i) {
            aux <- Q[,i];  Q[,i] <- Q[,j];  Q[,j] <- aux
            aux <- B[,i];  B[,i] <- B[,j];  B[,j] <- aux
        }
        d <- gcdv(B[,i])*sign(B[i,i])
        Q[,i] <- Q[,i]/d
        B[,i] <- B[,i]/d
        P <- diag(nc)
        for (j in (i+1):nc) {
            if (B[i,j] != 0) {
                s <- sign(B[i,j])
                P[i,j] <- s*B[i,j];  P[j,j] <- -s*B[i,i]
                B[,j] <- B[,i]*P[i,j] + B[,j]*P[j,j]
            }
        }
        Q <- Q %*% P
    }
    Q1 <- Q[,1:nr]
    P <- diag(nc)
    k <- nr + 1
    for (i in 1:nr) {
        if (prod(Q1[i,] == 0) == 1) {
            while (prod(Q1[k,] == 0) == 1) k <- k+1
            Q <- permrow(Q,i,k)
            P <- permrow(P,i,k)
            k <- k+1
            if (k >= nc) break
        }
    }
    for (i in (nr+1):nc) {
        idx <- (i-1)+order(Q[i:nc,i],decreasing=T)
        Q[i:nc,] <- Q[idx,]
        P[i:nc,] <- P[idx,]
    }
    return(list(Q=Q, B=B, perm=P%*%1:nc))
}

permrow <- function(Q,i,j) {
    row <- Q[i,]
    Q[i,] <- Q[j,]
    Q[j,] <- row
    return(Q)
}

gcd2 <- function(a,b) {ifelse(b == 0, a, gcd2(abs(b), abs(a%%b)))}
gcdv <- function(v) {
    n <- length(v)
    d <- gcd2(v[1],v[2])
    for (i in 3:n) {
        d <- gcd2(d,v[i])
    }
    return(d)
}

library(linprog)
library(quadprog)
network_mcmc <- function(y,A,x0=NULL,prior="beta",nits=100,burnin=20000,verbose=FALSE,fname="bhaas_chain_00.txt",hid=NULL) {
    nr <- nrow(A)
    nc <- ncol(A)
    hnfA <- hnf(A)
    inform <- is.numeric(x0) || !is.null(hid)
    Q2 <- hnfA$Q[1:nr,(nr+1):nc]
    y <- as.matrix(y,nrow=nr)
    ync <- ncol(y)
    rej <- 0
    for (k in 1:ync) {
        x1 <- round(forwardsolve(hnfA$B[,1:nr],y[,k]))
        b0 <- round(hnfA$Q[1:nr,1:nr] %*% x1)
        if (inform) {
            if (!is.null(hid)) x0 <- hid[k,]
            x0 <- x0[hnfA$perm]
            pt <- closestpt(Q2,b0,x0)
            x <- pt$x
            x[hnfA$perm] <- x
            if (prod(x >= 0) == 1) {
                write.table(x,fname,row.names=F,col.names=F,eol=" ",append=T)
                write.table("",fname,row.names=F,col.names=F,quote=F,append=T)
            } else {
                rej <- rej+1
            }
            iter <- 2
        } else {
            iter <- 1
        }
        xin <- rbind(b0, as.matrix(rep(0,nc-nr)))
        while (iter <= nits) {
            indim <- nc - nr
            dims <- sample.int(indim,indim)
            if (inform) {
                x <- isampleODC(Q2[,dims,drop=F],xin,pt$w[dims])
            } else {
                x <- usampleODC(Q2[,dims,drop=F],xin)
            }
            x[nr+dims] <- x[(nr+1):nc]
            x[hnfA$perm] <- x
            if (prod(x >= 0) != 1) {
                rej <- rej + 1
            } else {
                write.table(x,fname,row.names=F,col.names=F,eol=" ",append=T)
                write.table("",fname,row.names=F,col.names=F,quote=F,append=T)
                iter <- iter+1
            }
        }
    }
    cat("reject rate: ",rej/iter,"\n")
}

closestpt <- function(Q,b,x0) {
    nr <- nrow(Q)
    nc <- ncol(Q)
    x01 <- x0[1:nr]
    x02 <- x0[(nr+1):(nr+nc)]
    Qa <- rbind(Q,diag(nc))
    ba <- rbind(b,as.matrix(rep(0,nc),nc))
    Dmat <- crossprod(Q)+diag(nc)
    dvec <- t(Q)%*%(x01-b) + x02
    w <- solve.QP(Dmat,dvec,t(Qa),-ba)$solution
    for (i in 1:(nc)) {
        if(w[i] < 0) w[i] <- 0
    }
    b <- round(b + Q %*% w)
    x <- rbind(b, as.matrix(round(w),nc))
    return(list(x=x,w=w))
}


usampleODC <- function(Q, x) {
    pdim <- ncol(Q)
    x1dim <- nrow(Q)
    x1 <- x[1:x1dim]
    if (pdim <= 0) return(x)
    c <- rep(0,pdim)
    c[pdim] <- 1
    max <- -10000;  min <- 10000
    try(min <- ceiling(solveLP(c,x1,-Q,maximum=F)$opt))
    try(max <-   floor(solveLP(c,x1,-Q,maximum=T)$opt))
    if (max - min < 0) return(rep(-Inf,length(x)))
    cut <- floor(rbeta(1,(pdim+1)/2,(pdim+1)/2)*(max - min) + min)
    x[x1dim+pdim] <- cut
    x[1:x1dim] <- round(x1+cut*Q[,pdim])
    return(usampleODC(Q[,myseq(1,(pdim-1)),drop=F],x))
}

isampleODC <- function(Q,x,w) {
    minbetapar <- 0.05
    maxbetapar <- 20
    pdim <- ncol(Q)
    x1dim <- nrow(Q)
    x1 <- x[1:x1dim]
    if (pdim <= 0 ) return(x)
    c <- rep(0,pdim)
    c[pdim] <- 1
    max <- 10000; min <- 10000
    try(min <- ceiling(solveLP(c,x1,-Q,maximum=F)$opt))
    try(max <-   floor(solveLP(c,x1,-Q,maximum=T)$opt))
    if (max - min <= 0) return(rep(-Inf,length(x)))
    if (max == min) {
        cut <- max
    } else {
        alf <- (w[pdim] - min)*(pdim+1)/(max - min)
        alf <- min(maxbetapar,max(minbetapar,alf))
        bet <- min(maxbetapar,max(minbetapar,pdim+1-alf))
        cut <- floor(rbeta(1,alf,bet)*(max - min) + min)
    }
    x[x1dim+pdim] <- cut
    x[1:x1dim] <- round(x1+cut*Q[,pdim])
    return(isampleODC(Q[,myseq(1,(pdim-1)),drop=F],x,w))
}


mkgraph <- function(idx,sol,rootname) {
    H <- NULL
    for (i in idx) {
        fname <- paste(rootname,formatC(i,width=3,flag="0"),".txt",sep="")
        C <- as.matrix(read.table(fname))
        H <- rbind(H,C)
    }
    n <- ncol(H)
    nr <- floor(sqrt(n))
    nc = ceiling(n/nr)
    par(mfrow=c(nr,nc))
    for (i in 1:n) {
        hist(H[,i],breaks=40,xlim=range(max(0,min(H[,i])),max(H[,i])),xlab=paste("actual=",sol[i],sep=""),main=paste("coordinate",i))
        points(sol[i],0,pch=17)
    }
}

myseq <- function(min,max,by=1) {
    if ((max - min)*by >= 0) return(seq(min,max,by))
}

dist2 <- function(v) {return(sqrt(sum(v^2)))}

dist1 <- function(v) {return(sum(abs(v)))}

dist0 <- function(v) {return(max(abs(v)))}




positivize <- function(n,m,x0,Q) {
    perm <- 1:m
    for (i in 1:n) {
        if (x0[i] < 0) {
            for (j in 1:(m-n)) {
                if (Q[i,j] == 1) {
                    idx <- which(Q[,j] < 0)
                    idx <- idx[which(x0[idx] == min(x0[idx]))]
                    x0 <- x0 + x0[idx] * Q[,j]
                    Q[,j] <- -Q[,j]
                    sek <- c(myseq(1,(j-1)),myseq((j+1),(m-n)))
                    for (k in sek) {
                        if (Q[idx,k] == -1) Q[,k] <- Q[,k] + Q[,j]
                        else if (Q[idx,k] == 1) Q[,k] <- Q[,k] - Q[,j]
                    }
                    tmp <- Q[idx,]; Q[idx,] <- Q[n+j,]; Q[n+j,] <- tmp
                    tmp <- perm[idx]; perm[idx] <- perm[n+j]; perm[n+j] <- tmp
                    tmp <- x0[idx]; x0[idx] <- x0[n+j]; x0[n+j] <- tmp
                }
                if (x0[i] > 0) break
            }
        }
    }
    return(list(x0=x0,Q2=Q,perm=perm))
}

getvert <- function(n,m,x0,Q) {
    vert <- matrix(0,m,(m-n+1))
    newprob <- positivize(n,m,x0,Q)
    x0 <- newprob$x0
    Q <- newprob$Q2
    vert[newprob$perm,1] <- x0
    for (i in 1:(m-n)) {
        idx <- which(Q[,i] < 0)
        idx <- idx[which(x0[idx] == min(x0[idx]))]
        #cat("idx = ",idx,"\n")
        vert[newprob$perm,i+1] <- x0 + x0[idx] * Q[,i]
    }
    return(vert))
}
