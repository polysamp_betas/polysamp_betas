## Extract the command line arguments given by your LSF script:
args <- commandArgs(TRUE)

prior <-as.character(args[1])
chainNum <- as.numeric(args[2])
nits <- 1
rateVerif <- 50/100

##########(1)  THE DATA
source("bhaas_1rout_data.R")

#extract all observed link counts (Kbytes/sec.)
ntp <- floor(nrow(r1link)/nlk)
obs <- NULL
for (toi in 1:ntp) {
    tp <- r1link[(nlk*(toi-1)+1):(nlk*toi),]
    y <- round(300*tp$value[1:nlk-1])
    obs <- cbind(obs,y)
}

#extract all observed source-dest counts
ntp <- floor(nrow(r1count)/nsd)
hid <- NULL
for (toi in 1:ntp) {
    tp <- r1count[(nsd*(toi-1)+1):(nsd*toi),]
    x <- round(300*tp$value)
    hid <- rbind(hid,x)
}

#########(2)  THE ALGORITHM
source("bhaas_myfunctions.R")

rootname <- "bhaas_chain_uninf_all_"

fname <- paste(rootname,formatC(chainNum,width=3,flag="0"),".txt",sep="")
file.create(fname)

network_mcmc(obs,A,prior=prior,nits=nits,fname=fname)

#random verif
sol <- as.matrix(read.table(fname))
nverif <- round(rateVerif * nrow(sol))
idx <- sample.int(nrow(sol),nverif)
cat("discrepancy = max|A*sol[i] - y|:\n")
for (i in idx) {
    d <- dist1((A %*% sol[i,] - obs[,i]))
    cat("d[",i,"] = ",d,"\n")
}

d1 <- 0; d2 <- 0
for (i in idx) {
    d2 <- d2 + dist2(sol[i,]-hid[(i),])/nverif
    d1 <- d1 + dist1(sol[i,]-hid[(i),])/nverif
}
cat("average estimate of \"good-guessing\":\n")
cat("d1 = ",d1," d2 = ",d2,"\n")
