\documentclass{article}

\usepackage{amsthm,amsmath,amssymb}
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage{natbib}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{hyperref}
\newcommand{\theHalgorithm}{\arabic{algorithm}}
\usepackage{icml2010}
% \usepackage[accepted]{icml2010}

\icmltitlerunning{Network Tomography and Integer Polytope Sampling}

\begin{document}

\twocolumn[
\icmltitle{Network Tomography and Integer Polytope Sampling}

\icmlauthor{Bertrand Haas}{bhaas@fas.harvard.edu}
\icmladdress{Harvard University, Department of Statistics,
 1 Oxford Street, Cambridge, MA 02138, USA}
\icmlauthor{Edoardo Airoldi}{airoldi@fas.harvard.edu}
\icmladdress{Harvard University, Department of Statistics,
 1 Oxford Street, Cambridge, MA 02138, USA}

\icmlkeywords{Hermite Normal Form, Total Unimodularity, Integer Polytope, Dirichlet Distribution}

\vskip 0.3in
]

\theoremstyle{plain}
\newtheorem{thm}{Theorem}[section]
\newtheorem{lem}{Lemma}
\newtheorem{cor}[thm]{Corollary}
\newtheorem{prop}[thm]{Proposition}

\begin{abstract}
Abstract last.
\end{abstract}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
The setting of the problem we aim to solve is that of a network where some nodes are end nodes called \emph{Origin-Destination} (or \emph{OD} nodes) and other nodes are \emph{router nodes}.
The vocabulary and the example we follow are taken from computer networks, but our method applies as well to other networks such as traffic networks and to related problems such as contingency table counts (see section ???).
We consider here OD nodes communicating between each others thru the network of routers.
The number of bytes (or other message unit) going in and out of every router to each of its connected node is recorded (\emph{link counts}), and the problem is to infer from these counts the counts of bytes between each pair of OD node (\emph{OD counts}).
We assume here that the Origin-Destination paths are deterministic, in the sense that the routers will always choose to route a message to the same next-node for a given destination.
This problem has been relatively well studied (see for instance \cite{A_ANT_03}, \cite{TW_BINT_98}, \cite{GB_FMT_01}, \cite{GH_PELDS_96}, \cite{CDWY_TVNT_00}).

For a given network a $ m \times n $ full rank network matrix $ A $ can be constructed whereby
\begin{itemize}
\item the columns $ A[,i] $ correspond to the OD pairs potentially communicating with each other,
\item the rows $ A[i,] $ correspond to links
\item and the entries $ A[i,j] $ are equal to $ 1 $ if link $ A[i,] $ is part of the path for the OD pair $ A[,j] $, and equal to $ 0 $ otherwise.
\end{itemize}
For $ k $ OD nodes there are exactly $ n = k^2 $ OD pairs (an OD node can communicate with itself).
The number of links $ m $ is assumed here to be no more than $ k ^2 $ (a reasonable assumption for most studied networks).
If we denote by $ y $ the $ m $-vector of sufficient link counts (not counting the ones that can be recovered from the others), and by $ x $ the vector of OD counts, the matrix $ A $ specifies the relation $ Ax = y $.
Since $ m \leq n $, this is an undetermined problem having usually multiple solutions.
Moreover, since the counts are clearly positive integers and that each OD counts is clearly bounded (for instance by the sum of all link counts), the number of solutions is finite (although usually extremely large).

In section \ref{sec:main-algo} we give a representation of the space of solutions as an integer polytope in an $ m \times (n-m) $ dimensional space and we present an algorithm to sample integer points, corresponding to solutions, approximately uniformly in this polytope.

In section ??? we apply this algorithm to infering random solutions to a time series of observed link counts (by time slices of $5$ minutes).

In section \ref{sec:prior_info} we consider the case where the OD count in some previous time slice is known to infer a better solution at the current time slice.

We use a data set from ??? where the real OD counts have been recorded as well as the link counts.
This allows us in section ??? to assess the accuracy of our method by computing the difference between the OD count prediction and the actual OD counts.

In section ??? we compare our method both in accuracy and speed to the best known methods.

Finally in section ??? we investigate other applications to our method, in particular the inference of counts in contigency tables given marginal counts.

\subsection{Our Main Example}
\label{ssec:intro_exple}
We apply our method to the simple example from \cite{CDWY_TVNT_00} which is a small network with $ 4 $ OD nodes and one router; we investigate how it scales up to larger networks in section ???.
The OD nodes are $ a,b,c $ and $ d $, and the they are all connected to router $ r $ (see figure \ref{sple_netw_diagr}).

\begin{figure}[!h]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[scale=.6]{sple_netw_diagr.png}}
\caption{a simple network.\label{sple_netw_diagr}}
\end{center}
\vskip -0.2in
\end{figure} 

So the $ 7 \times 16 $ network matrix $ A $ has the form
\begin{equation}
\label{matrix_A}
\tiny
\hspace{-1em}\left(\begin{array}{cccccccccccccccc}
1  & 1  & 1  & 1  &    &    &    &    &    &    &    &    &    &    &    &    \\
&    &    &    & 1  & 1  & 1  & 1  &    &    &    &    &    &    &    &    \\
&    &    &    &    &    &    &    & 1  & 1  & 1  & 1  &    &    &    &    \\
&    &    &    &    &    &    &    &    &    &    &    & 1  & 1  & 1  & 1  \\
1  &    &    &    & 1  &    &    &    & 1  &    &    &    & 1  &    &    &    \\
& 1  &    &    &    & 1  &    &    &    & 1  &    &    &    & 1  &    &    \\
&    & 1  &    &    &    & 1  &    &    &    & 1  &    &    &    & 1  &    
\end{array}\right)
\end{equation}
where the zero entries are left blank for clarity, the columns can be indexed by the OD pairs  $ aa, ab, ac, ad, ba, bb, bc, bd, ca, cb, cc, cd, da, db, dc, dd $ and the rows by the links $ ar, br, cr, dr, ra, rb, rc $.
Notice also that the row corresponding to the link $ rd $ is absent since the counts can be recovered from the other links by conservation of the flow, and we assume $ A $ has maximal rank (it is easy to verify that $ A $ does have maximal rank).
In section \ref{sec:uninf_results} we solve the problem: Given an observed link count vector $ y $, sample fast solutions $ x $ form the system $ Ax = y $.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{The Main Algorithms}
\label{sec:main-algo}

In the more general case, the problem under consideration is:
\begin{center}
Sample integer solutions of the integer system
\begin{equation}
\label{initprob}
A\,x = y \quad\mbox{with}\quad x \geq 0
\end{equation}
\end{center}
where A is an $ m \times n $ network matrix (with entries $ 0 $ or $ 1 $), $ m \leq n $, and $ y $ is positive integer vector.
The Hermite Normal form of $ A $, is the square lower triangular integer matrix $ B_1 $ such that
\[
A\, Q = B 
\]
where $ Q $ is an integer $ n \times n $ matrix and $ B = [B_1 | 0 ] $ is $ m \times n $.
See for example \cite{C_CCANT_00} or \cite{S_TLIP_98}.
One of the main advantage of this decomposition over, for instance, the $ Q\,R $ decomposition, is that the integer (or {\em lattice}) structure of a problem $ A\,x = y $ is preserved.
Moreover, let's write $ Q = [Q_1|Q_2] $ where $ Q_1 $ is $ n \times m $.
It follows from this decomposition that the columns of $ Q_2 $ generate the null-space (or kernel) of $ A $.
Moreover in the HNF decomposition of a network matrix of the type of $ A $,  the matrix $ B $ has only $ 0 $ and $ \pm 1 $ entries, and the matrix $ Q $ can be transformed by suitable permutation of its rows (or of the columns of $ A $) into a block matrix
\[
\left(\begin{array}{cc} Q_{11} & Q_{12} \\ Q_{21} & Q_{22} \end{array}\right)
\]
where $ Q_{11} $ and $ Q_{12} $ have only $ 0 $ or $ \pm 1 $ as entries and $ Q_{12} = 0 $ and $ Q_{22} $ is the identity matrix (see figure \ref{hnf_matrices}).


\begin{figure}[!h]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[width=\columnwidth]{HNF_matrices.png}}
\caption{The Hermite Normal Form.\label{hnf_matrices}}
\end{center}
\vskip -0.2in
\end{figure} 

Finding integer solutions of problem (\ref{initprob}) decomposes into the following process: 
\begin{itemize}
\item Solve $ A\,Q\,x' = y $ where $ x' = Q^{-1}x = \binom{x'_1}{w} $ and $ x'_1 $ and $ w $ are respectively $ m $ dimensional and $ n-m $ dimensional vectors.
Since $ B = [B_1 | 0] $ this is equivalent to solving the square lower triangular system $ B_1\,x'_1 = y $.
\item Let $ b = Q_{11}\,x'_1 $.
The initial system became now a $ n \times (n-m) $ integer system
\[
\binom{b}{0} + \binom{Q_{12}}{I}w \geq 0
\]
\end{itemize}
Since this implies that $ w \geq 0 $, the initial problem has been transformed into an $ m \times (n-m) $ integer programming problem:
\begin{center}
Sample integer solutions of the integer system
\begin{equation}
\label{transprob}
b + Q_{12}\,w \geq 0,\quad\mbox{with}\quad w \geq 0
\end{equation}
\end{center}

Notice that this is equivalent to finding integer points of the polytope $ P $ defined by the inequalities.
We propose the following algorithm to solve (\ref{transprob}):

Function \textbf{SampleSol(Q,x)}\\
\textbf{Input:}  An integer $ m \times d $ matrix $ Q $ and a integer $n$-vector $ x $ (initially $ x $ is equal to the $ b $ of equation \ref{transprob}).
\begin{enumerate}
\item If $ d = 0 $ return $ x $
\item Choose a dimension $ i $ uniformly at random in $ [1,d] $.
\item \label{samplesol_step:min_max} Find the largest, $ max $, and smallest, $ min $, $i$-coordinate of points of $ P $.
\item \label{samplesol_step:sample_coord} Sample a coordinate $ x[m+i] $ (preferably using a Beta($\frac{d+1}{2},\frac{d+1}{2}$) distribution rescaled between $ min $ and $ max $ and rounded to an integer;
see justifitation below).
\item Let $ x[1:m] \leftarrow x[1:m] + x[m+i]\,Q[,i] $ where $ Q[,i] $ is the $ i^{\textrm{th}} $ column of $ Q $.
\item Let $ Q \leftarrow Q[,-i] $ where $ Q[,-i] $ is the $ n \times (d-1) $ matrix obtained from $ Q $ by removing its $ i^{\textrm{th}} $ column.
\item return SampleSol(Q,x)
\end{enumerate}
\textbf{Output:} A solution to the system (\ref{initprob}).

Step \ref{samplesol_step:min_max} can be done for example by the simplex method; the vector representing the function to optimize is extremely simple: the $ i^{\textrm{th}} $ base vector $ e_i $.

In step \ref{samplesol_step:sample_coord} one could sample each coordinate uniformly, which would be the right thing if the polytope was a hypercube.
However, in general sampling from the uniform distribution would over-represent points closer to the two extreme coordinate values and under-represent points in the middle.
For a $ d $ dimensional polytope $ P_d $, the right distribution along a dimension $ x_i $ would have the probabilities $ p(x_i = j) $ proportional to the number of integer points (roughly, the volume) of the sectioned $ d-1 $ polytope $ P_{d-1} = P_d \cap \{x_i = j\} $.
Since we do not know anything about the $ P_d $'s shape, we can assume an ``average'' shape close to a $ d $ dimensional ball (the $ d-1 $ sphere together with its interior).
So we can use the following lemma:
\begin{lem}
The following algorithm almost surely samples uniformly points in the $ d $ dimensional ball $ B_d $ (of arbitrary radius).
\begin{itemize}
\item Along an arbitrary dimension $ i $, choose a coordinate $ x_i = a $ at random using the Beta($\frac{d+1}{2}, \frac{d+1}{2}$) distribution rescaled from the South pole to the North Pole along that dimension.
\item repeat on the $ d-1 $ dimensional sphere $ B_{d-1} $ sectioned from $ B_d $ by the hyperplane $ x_i = a $ until $ d = 0 $
\end{itemize}
The point $ (x_1, x_2, \dots, x_d) $ constructed this way is a uniform sample on $ B_d $.
\end{lem}
\begin{proof}
Let $ f = (f_{x_1}, f_{x_2}, \dots, f_{x_d}) $ be the pdf, and its coordinate components, of the uniform distribution on the ball $ B_d $.
The component pdf $ f_{x_i}(a) $ must be proportional to the volume $ V_a $ of the ball $ B_{d-1}(a) $ that is the section of $ B_d $ by $ x_i = a $.
Without loss of generality we can assume $ B_d $ has radius $ 0.5 $ and is centered at $ (0.5, 0.5, \dots, 0.5) $.
So the defining equation for $ B_d $ is 
\[
(x_i-0.5)^2 + \sum_{j\not=i}(x_j-0.5)^2 \leq 0.25
\]
Therefore the radius of $ B_{d-1}(a) $ is 
\[
\sqrt{0.25 - (a-0.5)^2} = \sqrt{a - a^2} = a^{1/2}(1-a)^{1/2} 
\]
and its volume 
\[
V_a \propto a^{\frac{d-1}{2}}(1-a)^{\frac{d-1}{2}}
\]
Therefore the pdf must be the pdf of the Beta($\frac{d+1}{2},\frac{d+1}{2}$) distribution.
\end{proof}

We now describe the Hermite Normal Form (HNF) algorithm.
This is an adaptation of the algorithm in \cite{C_CCANT_00}.

Function \textbf{hnf(A)}\\
\textbf{Input:} An integer $ m \times n $ matrix ($ m \leq n$) of full rank.
\begin{enumerate}
    \item Initialize $ Q \leftarrow I_n $ (the $ n \times n $ identity matrix) and $ B \leftarrow A $
    \item From row $ i = 1 $ to $ m $ do:
    \begin{enumerate}
        \item If necessary permutate columns $ i $ and column $ k $ for some $ k > i $ so that the $ B[i,i] \not = 0 $ and record the permutation in $ Q $ (if $ B[i,k] = 0 $ for all $ k \geq i $ the rank of $ A $ was not full.)
        \item Divide all entries of $ B[,i] $ by their gcd and multiply $ B[,i] $ by $ -1 $ if becessary to make $ B[i,i] $ positive.  Record the transformation in $ Q $,
        \item For each column $ j = i+1 $ to $ n $ such that $ B[i,j] \not = 0 $ do:
        \begin{enumerate}
            \item If necessary multiply column $ B[,j] $ by $ -1 $  to make $ B[i,j] > 0 $.
            Then set $ B[,j] \leftarrow B[,i]\,B[i,j] - B[,j]\,B[i,i] $ so that the $ B[i,j] = 0 $ (like Gaussian elimination).
            Record the transformation in $ Q $.
        \end{enumerate}
    \end{enumerate}
\end{enumerate}
\textbf{Output:} Integer $ n \times n $ matrix $ Q $ and integer $ m \times n $ matrix $ B = [B_1|0] $ where $ B_1 $ is $ m \times m $ lower triangular.

\section{Computational Results on an Example}
\label{sec:uninf_results}
We continue the example introduced in section \ref{ssec:intro_exple}.
The given data consists in the matrix $ A $ shown in \ref{matrix_A} and the vector of observed link counts 
\[
y = \left(\begin{array}{c}
12053370 \\ 14424000 \\ 249121410 \\ 1485729 \\ 248498220 \\ 2365508 \\ 15357294
\end{array}\right)
\]
The HNF algorithm of section \ref{sec:main-algo} produces:
\begin{itemize}
\item The permutation of the rows of $ Q $ (or columns of $ A $): \\
$ (1,2,8,9,5,13,7,6,3,10,11,12,4,14,15,16) $
\item The matrices $ Q_{11} $ and $ Q_{12} $
\[
\hspace{-2em}\begin{array}{rl}
Q_{11} =&\!\!\!\! \scriptsize\left(\begin{array}{rrrrrrr}
1 &   &   &   & 1 & -1&   \\
  &   &   &   & -1& 1 &   \\
  &   &   &   &   &   & -1\\
  &   & 1 &   &   &   &   \\
  & 1 &   &   &   & 1 &   \\
  &   &   & 1 &   &   &   \\
  &   &   &   &   & -1& 1 
\end{array}\right) \\
\\
Q_{12} =&\!\!\!\! \scriptsize\left(\begin{array}{rrrrrrrrr}
1 & -1& 1 &   &   & -1& 1 &   &   \\
-1&   & -1&   &   &   & -1&   &   \\
  &   &   &   & -1& -1&   &   & -1\\
  &   & -1& -1& -1&   &   &   &   \\
-1& 1 &   & 1 & 1 & 1 &   & 1 & 1 \\
  &   &   &   &   &   & -1& -1& -1\\
  & -1&   & -1&   &   &   & -1&  
\end{array}\right)
\end{array}
\]
\item The matrix $ B_1 $
\[
B_1 = \scriptsize\left(\begin{array}{rrrrrrr}
1 &   &   &   &   &   &   \\
  & 1 &   &   &   &   &   \\
  &   & 1 &   &   &   &   \\
  &   &   & 1 &   &   &   \\
1 & 1 & 1 & 1 & 1 &   &   \\
  &   &   &   & -1& 1 &   \\
  &   &   &   &   & -1& 1
\end{array}\right)
\]
\end{itemize}
Next we solve the system $ B_1\,x'_1 = y $ and get the vector $ b = Q_{11}x'_1 $
\[
x_1 = \left(\begin{array}{c} 12053370 \\ 14424000 \\ 249121410 \\ 1485729 \\ -28586289 \\ -26220781 \\ -10863487 \end{array}\right)\qquad
b = \left(\begin{array}{c} 9687862 \\ 2365508 \\ 10863487 \\ 249121410 \\ -11796781 \\ 1485729 \\ 15357294 \end{array}\right)
\]
And from there on, we use the function SampleSol($Q_{12},b$) to generate samples of solutions, like:
\[
\left(\begin{array}{c}
4699903 \\ 193924 \\  2073216 \\  5086327 \\  7058077 \\   687677 \\  5405147 \\  1273099 \\ 236524405 \\  1033162 \\ 7605036 \\  3958807 \\   215835 \\   450745 \\   273895 \\   545254 \end{array}\right)
\quad\mbox{or}\quad
\left(\begin{array}{c}
6386434 \\   316730 \\  1270186 \\  4080020 \\  7497193 \\   278638 \\  5518334 \\  1129835 \\ 234466383 \\ 854497 \\ 8436183 \\  5364347 \\   148210 \\   915643 \\   132591 \\   289285 \end{array}\right)
\]

We ran the algorithm on a cluster of machines to produce $ 10 $ independent chains of $ 100,000 $ solutions with a CPU time of about $ 9 $ hours.
The histograms of the coordinates are plotted on figure (\ref{uninfmarginals}).
Notice that the actual solution is, for most of its coordinates, away from the averages.
This means that the actual solution is situated somewhere on the boundary of the polytope.

\begin{figure}[!h]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[width=\columnwidth]{bhaas_1router_t5_uniform.pdf}}
\caption{Histograms of the OD solutions coordinates in the uninformative case.  The black triangles on the x-axis point to the coordinates of the actual solution from the data set.\label{uninfmarginals}}
\end{center}
\vskip -0.2in
\end{figure} 

The sampled solutions are exact in the sense that $ A $ times a solution gives exactly the observation.
The average distance between the sampled solutions and the actual solution is of the order:
\begin{itemize}
\item $L_1$-distance: $ 55 $ Million counts.
\item $L_2$-distance: $ 20 $ Million counts.
\end{itemize}

\section{Adjusting the Algorithm for Prior Information}
\label{sec:prior_info}
OD-messages often contain a large load of information that takes more than 5mn to be transmitted.
Therefore we can expect two consecutive OD solutions to be in average rather correlated.
In a model where we know the previous solution, we can then tweak the distribution so that the average is as close as possible from the previous solution.
Clearly the previous solution $ x_0 = \binom{x_{01}}{w_0} $ is a solution from a different system (the previous link counts differ from the current ones), so $ w_0 $ usually lies outside the kernel polytope $ P $ (defined by (\ref{transprob})).
To find a solution $ x $ from the current system that minimizes the distance with $ x_0 $ we minimize the quadratic function:
\begin{eqnarray*}
||x - x_0||^2 &=& ||Q\,z + b - x_0||^2 \\
    &=& ||b-x_0||^2 + 2(b - x_0)^TQ_2x + x^TQ_2^TQ_2x
\end{eqnarray*}
which amounts to minimizing $ f(x) = (2(b - x_0)^TQ_2)\cdot x + x^T(Q_2^T Q_2)\,x $.
This is done, for instance, using some quadratic programming package.

Let $ w_1 \in P $ be the polytope point that minimizes the distance between $ x $ and $ x_0 $.
We can now sample more points from the polytope in the vicinity of $ w_1 $ by setting the beta distributions from section \ref{sec:main_algo} so that their means correspond to the coordinates of $ w_1 $.
Figure (\ref{informmarginals}) shows the histograms from samples obtained this way.
Compare with histograms (\ref{uninfmarginals}), and notice how the actual solution corresponds to the average of the distributions.

\begin{figure}[!h]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[width=\columnwidth]{bhaas_1router_t5_informative.pdf}}
\caption{Histograms of the solutions coordinates at time point $5$ in the case where the OD solution at time $4$ is known.\label{informmarginals}}
\end{center}
\vskip -0.2in
\end{figure} 

$ 10 $ chains of $ 10,000 $ samples were generated on Odyssey, each taking about $ 3,000 $ seconds, that is, $ 50 $ minutes.
Sampled solutions are practically exact:
Over $ 100,000 $ samples, the largest discrepancy between an $ A\,x $ and the observation is of $ 1 $ count and there were $ 3 $ such cases.
All the rest are exact solutions.
The average distance between the sampled solutions and the actual solution is of the order of:
\begin{itemize}
\item $L_1$-distance: $ 4.5 $ Million counts.
\item $L_2$-distance: $ 2 $ Million counts.
\end{itemize}
Compared with the uninformed case this is about a $ 90\% $ improvement.\\

\section{A Time Series Model}

\texttt{description of the model}

Using the actual solutions for each of the $ 287 $ observed link counts in the data set we can verify how well our prediction do in average in the informative and uninformative case.

The uninformative algorithm (of section \ref{sec:main-algo}) already produces OD counts that are strikingly similar in some coordinates (for instance $7$ and $9$) to the actual OD counts:  compare figures (\ref{allactual}) and (\ref{alluninf}).
All solutions appear to be exact.
The average distance between the sampled solutions and the actual OD counts is around
\begin{itemize}
\item $L_1$-distance: $ 46 $ Million counts
\item $L_2$-distance: $ 16 $ Million counts
\end{itemize}

The informative algorithm here only computes the $ l_2 $-closest solution for each time point, with the previous actual OD-count as prior information.
Since it does not sample solutions it runs much faster than the uniniformative algorithm.
Solutions tend not to be all exact (many have discrepancies of $ 2 $, $ 3 $, or even $ 4 $ counts between the calculated and the observed data.)
The average distance between the closest solutions and the actual OD-counts are about:
\begin{itemize}
\item $L_1$-distance: $ 17 $ Million counts.
\item $L_2$-distance: $ 6 $ Million counts.
\end{itemize}

\begin{figure}[ht]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[width=\columnwidth]{bhaas_1router_all_times_actual.pdf}}
\caption{Plots of the actual OD counts against time ($287$ time points).\label{allactual}}
\end{center}
\vskip -0.2in
\end{figure} 

\begin{figure}[ht]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[width=\columnwidth]{bhaas_1router_all_times_uninform.pdf}}
\caption{Plots of the OD counts generated from the uninformed algorithm.\label{alluninf}}
\end{center}
\vskip -0.2in
\end{figure} 

\begin{figure}[ht]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[width=\columnwidth]{bhaas_1router_all_times_informative.pdf}}
\caption{Plots of the OD counts generated from the informed algorithm adapted from section \ref{uninfalgo}.\label{allinform}}
\end{center}
\vskip -0.2in
\end{figure} 

%\appendix
%\section{Details of variational inference}
%\label{app:blah}
%\subsection{Variational objective}
%\section*{Acknowledgements}


\bibliography{bibtex/1.bertrand_references.bib}
\bibliographystyle{icml2010}

\end{document}
