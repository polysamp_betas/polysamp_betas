\documentclass{article}

\usepackage{amsthm,amsmath,amssymb}
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage{natbib}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{hyperref}

\title{Integer Point Sampling in high dimensional Polytopes}

\author{Bertrand Haas, Edoardo Airoldi\\ Harvard University, Department of Statistics}

\theoremstyle{plain}
\newtheorem{thm}{Theorem}[section]
\newtheorem{lem}{Lemma}
\newtheorem{cor}[thm]{Corollary}
\newtheorem{prop}[thm]{Proposition}

\newcommand{\Vol}{\mbox{Vol}}


\begin{document}
\maketitle
\begin{abstract}
Under-determined linear systems $ Ax = b $ with non-negative solutions $ x \geq 0 $ have a solution space that is a {\em polyhedron}.
This is the typical setting for Linear Programming where the problem is to optimize a linear function.
In several distinct applications, however, the problem is to sample points in the solution space.
We mention here $3$ examples of such applications where the solution polyhedron is bounded (a {\em polytope}).
We therefore take up here the problem of sampling uniformly from such polytopes.
Sampling algorithms that relie on MCMC and the Metropolis-Hasting methods have been proposed (see \cite{} \cite{}), but seem rather inefficient already in modest dimensions.
Faster algorithms, described in \cite{}, firstly require to compute the vertices of the polytope.
Even though this first step is done efficiently, because in higher dimension there might just be too many vertices for a computer to handle, it is still limited to relatively low dimensions.
We propose here a new fast sampling algorithm that bypasses the computation of all the vertices and is therefore not constrained by the dimension of the problem.
The drawback is that the samples are produced according to an approximate uniform distribution, but insights in the method and comparative tests reveal that the distribution is practically indistinguishable from a uniform one.
\end{abstract}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
\subsection{Examples of applications}
We quickly review three examples in three active areas of research, network tomography, contingency tables and flux-balance analysis, and how the underlying problem can be viewed as the sampling of points in high dimensional polytopes.
For more details in these applications we refer to the litterature here cited.

A typical example in network tomography is that of a controlable physical network of computers and routers (\cite{}, \cite{}, and \cite{}) where data (say paquets) transits from computer to computer through routers in a deterministic way.
A typical application would be to build a network that optimizes the flow of data between some subset of computers.
For that purpose one would ideally like to know the amount of data transiting from every computer to every computer.
However one can reasonably only have the routers record the amount of data transiting from and to the next ones, at several time points.
Since the data from one computer transiting in each link is usually mixed with data from other computers, we need to {\em deconvolute} the recorded data.
This can be done in several ways, leading to a finite (but usually very large) solution space.
The mathematical model is precisely $ Ax = b $ where
\begin{itemize}
\item the matrix $A$ is the incidence matrix between the $m$ links and all the $n$ pairs of computers ($n$ assumed larger than $m$).
\item $b$ is the traffic in each link (recorded by the routers).
\item $x$ is a possible solution, that is a vector of traffic between each pair of computers.
\end{itemize}
Typically the routers record the traffic at several time points.
From this one can infer a probability distribution on the $b$-space or directly on the $x$-space.
Or from some experiment, one might compute the likelihood of the ground truth $x$ for several different networks.
See \cite{} and \cite{} for several variations on this problem.
In any case an efficient way of sampling from the space of solutions from $ Ax = b $ is a necessary tool.

For contingency tables, one typically investigate how several variables are dependent (\cite{}, \cite{} and \cite{}).
A typical setting is for a two-way table where a null hypothesis is that the two variables (respectively with $m_1$ and $m_2$ levels) are assumed independent.
In that case the probability of an observation to fall in a cell is the product of the marginal probabilities.
The marginal probabilities $ \hat{p}_{i+} $ and $ \hat{p}_{+j} $ and the observed probabilities in each cells $ \hat{p}_{ij} $ are easily infered.
Then a chi-squared value between the $ \hat{p}_{ij} $ and the products $ \hat{p}_{i+} \hat{p}_{+j} $ is computed.
Typically this value should be compared to chi-squared values computed from other tables with same marginals (in oreder to compute a p-value).
Since there are typically too many tables with same marginals, one needs an efficient way to sample such tables.
The mathematical setting is again a system $ Ax = b $ where
\begin{itemize}
\item the $ x $ are vectorized tables.
\item $ b $ is the vector of marginal values.
\item $ A $ is the incidence matrix between the $ m_1 + m_2 $ marginal values and the $ m_1 m_2 $ entries in the table.
\end{itemize}
And the problem is again to efficiently sample, usually uniformly, solutions from this system.

A typical problem in flux-balance analysis, is to infer rate of known reactions in a proposed biological network of cellular reactions (the metabolic model) from known concentrations of metabolites (\cite{}, \cite{} and \cite{}).
The concentrations of metabolites in the cell is known from previous experiment.
Typically the in-flux of a few nutrient metabolites (such as oxygen, glucose) is controlled and the out-flux of some product metabolites (such as $ \mbox{CO}_2 $) is observed.
At equilibrium (in homeostatic regime) the flux of other metabolites should be zero.
The mathematical setting is once more a system $ Ax = b $ where
\begin{itemize}
\item $ A $ is the {\em stoichiometric} matrix with entries $ A[i,j] $ equal to the number of molecule of metabolite $ i $ entering (with a positive sign) or resulting from (with a negative sign) the equation of reaction $ j $.
\item $ b $ is zero everywhere except at the indices corresponding to the nutrient and product metabolites.
\item $ x $ is the unknown vector of rates.
\end{itemize}
Since there are more reactions than metabolites, the system is under-determined.
Often the goal is to optimize some objective function (such as biomass or the rate of synthesis of some product).
If the objective function is linear, linear programming is the tools of choice.
For more complex objective functions, MCMC and Metropolis-Hasting algorithms based on sampling the solution space can be used to find optima.
Sometimes extra constraints, such as minimum and maximum rates, are added to produce a smaller range of possible rates.
Instead of such ``hard'' constraints, probability distributions over the rates might be set, producing a probability distribution over the space of solutions.
In any case, here again, efficiently sampling solutions from this system is a key step.

\subsection{Algorithms for this problem}
Algorithms relying on MCMC or the Metropolis-Hasting algorithm have been proposed to sample uniformly from such solution space (see \cite{}, \cite{} and \cite{}), but they all suffer from their sequential nature and seem to become increasingly inefficient as the dimension of the problem increases.
Algorithms proposed in \cite{}, require to compute the vertices of the solution polytope.
This extra computation done, points can immediately be sampled independently.
The sampling can therefore be massively parallelized, speeding up the process considerably.
However, once again, as the dimension increases, the number of vertices seem to increase in the worst case exponentially.
For example a $4$ by $4$ by $4$ contingency table, with given $2$-dimensional marginals, yields a matrix $ A $ of dimesnion $ 37 $ by $ 256 $ with a solution polytope which in the worst case has a number of vertices of the order of $ 10^35 $.
Therefore there is a need for an algorithm that does not require to compute the vertices of the solution polytope.

We propose here an algorithm that finds coordinates of a sample solution one at a time and uses the information from previously found coordinates in order to find the next one (therefore in the Gibbs sampler spirit).

\section{Sampling Integer Points in an Integer Polytope}
\label{sec:SamplePtPoly}
Let $ P $ be an integer polytope described by the system
\begin{equation}
\label{PolytopeIneqs}
Ax \geq b
\end{equation}
Where $ A $ is an $ m \times n $ matrix, $ m > n $, and the entries of both $ A $ and the vector $ b $ are in $ \mathbb{Z} $.

For sampling integer points from $ P $ we will make use of uniform sampling from a ball 
(a sphere together with its interior).
We write $ B_n(a,r) $ for the $n$-dimensional ball with center $ a $ and radius $ r $, or simply $ B_n $ when $ a = 0 $ and $ r = 1 $.

\begin{lem}
Algorithm \ref{SamplePtBall} almost surely samples points uniformly in the $ n $ dimensional ball $ B_n(v,r) $, for arbitrary $ v $ and $ r $.
\end{lem}
\begin{algorithm}[tb]
\caption{SamplePtBall($v$,$r$)}
\label{SamplePtBall}
\begin{algorithmic}
\REQUIRE a $n$-dimensional ($ n > 0 $) vector $ v $ representing the center of a ball. \\
    a real $ r $ representing the radius of the ball.
\ENSURE a point in the ball $ v+r\,B_n $. \\
--------------------------------------------------
    \STATE Set $ \ell = v[1] + r $ and $ s = v[1] - r $.
    \STATE Set $ a $ a real value sampled at random between $ \ell $ and $ s $ using the beta distribution $ \mbox{Beta}(\frac{n+1}{2}, \frac{n+1}{2}) $.
    \IF{$ n == 1 $}
        \STATE {\bf return} $ a $
    \ELSE
        \STATE Set $ v' = v $ then $ v'[1] = a $
        \STATE Set $ r' = \sqrt{r^2 - (a - v[1])^2} $
        \STATE Set $ x = \mbox{SamplePtPoly}(v',r') $
        \STATE Modify $ x $ by pre-pending $ a $ to it.
        \STATE {\bf return} {$x$}
    \ENDIF
\end{algorithmic}
\end{algorithm}
\begin{proof}
Without loss of generality we can assume the ball has radius $ r = 0.5 $ and is centered at $ v = (0.5, 0.5, \dots, 0.5) $.
Let $ X_1, X_2, \dots, X_n $ be the random variables equal to the coordinates of points sampled uniformly in $ v + rB_n $, and let $ f_{X_1}, f_{X_2}, \dots, f_{X_n} $ be their associated pdf.
The pdf $ f_{X_i}(a) $ must be proportional to the volume $ V_a $ of the ball $ v'+r'B_{n-1} $ that is the section of $ v + rB_n $ by the hyperplane $ x_i = a $.
Writing the defining equation for $ v + rB_n $
\[
(x_i-0.5)^2 + \sum_{j\not=i}(x_j-0.5)^2 \leq 0.25
\]
we see that
\[
r' = \sqrt{0.25 - (a-0.5)^2} = \sqrt{a - a^2} = a^{1/2}(1-a)^{1/2} 
\]
and its volume
\[
V_a \propto a^{\frac{n-1}{2}}(1-a)^{\frac{n-1}{2}}
\]
Therefore the pdf must be the pdf of the Beta($\frac{n+1}{2},\frac{n+1}{2}$) distribution.
\end{proof}

\begin{lem}
Algorithm \ref{SamplePtPoly} samples integer points approximately uniformly in an $ n $ dimensional integer polytope $ P \subset \mathbb{R}^n $.
\end{lem}
The meaning of ``relatively uniformly'' is explained in the proof-discussion below.
See an illustration of the algorithm on figure \ref{SamplePtPoly_fig}.
\begin{algorithm}[tb]
\caption{SamplePtPoly($A$,$b$)}
\label{SamplePtPoly}
\begin{algorithmic}
\REQUIRE
    a matrix $ A $ with $ n > 0 $ columns $ A[,i] $\\
    a vector $ b $
\ENSURE a particular solution of $ Ax \geq b $. \\
--------------------------------------------------
    \STATE {\bf (1)} Choose uniformly at random $ 1 \leq i \leq n $.
    \STATE {\bf (2)} Determine $ \ell $ and $ s $ respectively the largest and the smallest $i$-coordinates from points $ p \in P $ (using for instance linear programming)
    \STATE {\bf (3)} Set $ x_i $ an integer value sampled at random between $ \ell $ and $ s $ using the beta distribution $ \mbox{Beta}(\frac{n+1}{2}, \frac{n+1}{2}) $.
    \IF{$ n == 1 $}
        \STATE {\bf (4)} {\bf return} $ x_i $
    \ELSE
        \STATE {\bf (5)} Set $ b' = b - x_i\,A[,i] $ and $ A' $ the matrix resulting from removing the column $ A[,i] $ from $ A $.
        \STATE {\bf (6)} Set $ x = \mbox{SamplePtPoly}(A',b') $
        \STATE {\bf (7)} Modify $ x $ by inserting $ x_i $ between its $(i-1)^{\textrm{th}}$ and $i^{\textrm{th}}$ coordinates.
        \STATE {\bf (8)} {\bf return} {$x$}
    \ENDIF
\end{algorithmic}
\end{algorithm}

\begin{figure}[!h]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[width=\columnwidth]{polytope_ball_3dim.png}}
\caption{The sampling algorithm from dimension $3$ on.\label{SamplePtPoly_fig}}
\end{center}
\vskip -0.2in
\end{figure} 
Step (3) can be done for instance by the simplex method.
The function to optimize being extremely simple (a basis vector) and only the value of the function being of interest (not the point) there might be room for optimization.
\begin{proof}
\textit{(and discussion).}
As for the ball, for the distribution of the sample point to be uniform, the distribution of each of its coordinate (say $ x_n $) should be proportional to the distribution of the number of integer points in the cut polytopes $ P_{n-1} = P_n \cap \{x_n = a\} $.
Computing the exact number of integer points in a polytope is prohibitive, but it should be roughly proportional to the volume of the polytope.
However, unlike for the ball, we do not know in advance the shape of the polytope, and neither its volume.
We therefore approximate it by a prior shape (in Bayesian language), the shape of a ball.
This gives us an approximate distribution for the first coordinate sample.

By sampling coordinate by coordinate, the algorithm constructs a flag of polytopes (a chain of polytopes $ P_n \supset P_{n-1} \supset \dots \supset P_1 \supset P_0 $ with $ \dim{P_i} = i $, and $ P_0 $ the sample point).
For one sampling we can assume without loss of generality that the sampling proceeds from coordinates $ x_n $ to $ x_{n-1} $ to \dots to $ x_1 $.

The first approximate distibution, for $ x_n $, might be rather off.
This is due to the higher dimension of the subvolumes of the cut polytopes at $ x_n $.
The difference between the distribution of $ \Vol(P_{n-1}) $ along the $ n^{\textrm{th}} $ dimension and the distribution of $ \Vol(B_{n-1}) $ is of the order of the $ n-1 $ power of $ x_n $.
This is easily seen in the extreme case where $ P_n $ is the standard cube centered at the origin:  the distribution is uniform, while the distribution for the balls is proportional to $ 1 - x_n^{n-1} $.

This kind of error in the distribution of the $ x_n $ tends to recede in the distribution of subsequent coordinates for two main reasons:
\begin{itemize}
\item As we go down in dimension the error becomes proportional to lesser powers of the sampled coordinate; down to dimension $ 1 $ where there is no discrepancy at all (a polytope of dimension $1$ is a segment is a ball of dimension $1$).
\item Because the cut polytopes make up a flag, if one $ P_i $ has a shape close enough to a ball $ B_i $, all the subsequent $ P_j $, $ 1 \leq j < i $ have much greater chance to also be close in shape to $ B_j $.
Therefore, from dimension $ i $ to $ 1 $ the distribution of subvolumes $ \Vol(P_i) $ has greater chance to be close to the distribution of subvolumes $ \Vol(B_i) $.
In other words, the probabilistic constraints for a flag of polytope to have all the $ P_i $ of a shape far away from a ball get higher as the dimension get higher.
\end{itemize}
Therefore we can expect the distributions for the first few coordinates to be somewhat biased toward the middle of the polytope (like for the ball) but the distributions for the subsequent coordinates to be much closer to the actual distributions.
We can call this the {\emph ball prior effect} and this results in a point distribution that is ``relatively uniform'' in this sense (more or less uniform relatively to which coordinates we consider).

For consecutive sampling, the randomization of the order of sampling among the coordinates solves this problem.
The higher the dimension (and of course the larger the size of the sampling), the better the dilution of the ball prior effect.
Therefore the uniformity of the point distribution is also relative to the dimension and the sample size.
\end{proof}

\section{Application to Network tomography}
\subsection{Introduction to the Problem}
The setting of the problem we aim to solve is that of a network where some nodes are end nodes called \emph{Origin-Destination} (or \emph{OD} nodes) and other nodes are \emph{router nodes}.
The vocabulary and the example we follow are taken from computer networks, but our method applies as well to other networks such as automobile traffic networks or cell phone networks and to related problems such as contingency table counts.
We consider here OD nodes communicating between each others thru the network of routers.
The number of bytes (or other message unit) going in and out of every router to each of its connected node is recorded (\emph{link counts}), and the problem is to infer from these counts the counts of bytes between each pair of OD node (\emph{OD counts}).
We assume here that the Origin-Destination paths are deterministic, in the sense that the routers will always choose to route a message to the same next-node for a given destination.
This problem has been relatively well studied (see for instance \cite{A_ANT_03}, \cite{TW_BINT_98}, \cite{GB_FMT_01}, \cite{GH_PELDS_96}, \cite{CDWY_TVNT_00} or \cite{K_SAND_09}).

For a given network a $ m \times n $ full rank network matrix $ A $ can be constructed whereby
\begin{itemize}
\item the columns $ A[,i] $ correspond to the OD pairs potentially communicating with each other,
\item the rows $ A[i,] $ correspond to links (usually not all, but just enough so that $ A $ has maximum rank),
\item and the entries $ A[i,j] $ are equal to $ 1 $ if link $ A[i,] $ is part of the path for the OD pair $ A[,j] $, and equal to $ 0 $ otherwise.
\end{itemize}
For $ k $ OD nodes there are exactly $ n = k^2 $ OD pairs (an OD node can communicate with itself).
The number of links $ m $ is assumed here to be no more than $ k ^2 $ (a reasonable assumption for most studied networks).
If we denote by $ y $ the $ m $-vector of link counts corrsponding to $ A $'s rows, and by $ x $ the vector of OD counts, the matrix $ A $ specifies the relation $ Ax = y $.
Since $ m \leq n $, this is an undetermined problem having usually multiple solutions.
Moreover, since the counts are positive integers and that each OD counts is clearly bounded (for instance by the sum of all link counts), the number of solutions is finite (although in practical applications usually extremely large).

\subsection{Our Main Example}
\label{ssec:intro_exple}
We apply our method to the simple example from \cite{CDWY_TVNT_00} which is a small network with $ 4 $ OD nodes ($a$, $b$, $c$, and $d$) and one router ($r$); see figure \ref{sple_netw_diagr}.

\begin{figure}[!h]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[scale=.6]{sple_netw_diagr.png}}
\caption{a simple network.\label{sple_netw_diagr}}
\end{center}
\vskip -0.2in
\end{figure} 

So the $ 7 \times 16 $ network matrix $ A $ has the form
\begin{equation}
\label{matrix_A}
\tiny
\hspace{-1em}\left(\begin{array}{cccccccccccccccc}
1  & 1  & 1  & 1  &    &    &    &    &    &    &    &    &    &    &    &    \\
&    &    &    & 1  & 1  & 1  & 1  &    &    &    &    &    &    &    &    \\
&    &    &    &    &    &    &    & 1  & 1  & 1  & 1  &    &    &    &    \\
&    &    &    &    &    &    &    &    &    &    &    & 1  & 1  & 1  & 1  \\
1  &    &    &    & 1  &    &    &    & 1  &    &    &    & 1  &    &    &    \\
& 1  &    &    &    & 1  &    &    &    & 1  &    &    &    & 1  &    &    \\
&    & 1  &    &    &    & 1  &    &    &    & 1  &    &    &    & 1  &    
\end{array}\right)
\end{equation}
where the zero entries are left blank for clarity, the columns are indexed by the OD pairs  $ aa, ab, ac, ad, ba, bb, bc, bd, ca, cb, cc, cd, da, db, dc, dd $ and the rows by the links $ ar, br, cr, dr, ra, rb, rc $.
It is easily verified that the rank is $ 7 $.  Therefore to make $ A $'s rank maximal we left out link $ rd $ (by conservation of flow, its counts can indeed be recovered from the other link counts).

\section{The Main Algorithms}
\label{sec:main-algo}
In the more general case, the problem under consideration is:
\begin{center}
Sample integer solutions of the integer system
\begin{equation}
\label{initprob}
A\,x = y \quad\mbox{with}\quad x \geq 0
\end{equation}
\end{center}
where A is an $ m \times n $ network matrix, $ m \leq n $, and $ y $ is a given positive integer vector.
The Hermite Normal form of $ A $, is the unique square lower triangular integer matrix $ B_1 $ such that
\[
A\, Q = B 
\]
where $ Q $ is an integer unimodular $ n \times n $ matrix and $ B = [B_1 | 0 ] $ is $ m \times n $.
See for example \cite{C_CCANT_00} or \cite{S_TLIP_98}.
If moreover we require that $ B[i.i] > 0 $ and $ 0 \leq B_1[i,j] < B_1[i,i] $ for $ i<j $, it can be shown that the $ B $ is uniquely defined.
Let's write $ Q = [Q_1|Q_2] $ where $ Q_1 $ is $ n \times m $.
It follows from this decomposition that the columns of $ Q_2 $ generate the null-space of $ A $.
Notice that, unlike for the $ Q\,R $ decomposition, the integer (or {\em lattice}) structure of a problem $ A\,x = y $ is preserved.

\begin{figure}[!h]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[width=\columnwidth]{HNF_matrices.png}}
\caption{The Hermite Normal Form.\label{hnf_matrices}}
\end{center}
\vskip -0.2in
\end{figure} 

Finding integer solutions to problem (\ref{initprob}) decomposes into the following process: 
\begin{itemize}
\item Solve $ AQ\,x' = y $ where $ x' = Q^{-1}x = \binom{x'_1}{w} $ and $ x'_1 $ and $ w $ are respectively $ m $ dimensional and $ n-m $ dimensional vectors.
Since $ AQ = B = [B_1 | 0] $ this is equivalent to solving the square lower triangular system $ B_1\,x'_1 = y $.
\item Let $ b = Q_1\,x'_1 $.
\end{itemize}
The initial problem has been transformed into an integer polytope sampling problem solved in section \ref{sec:SamplePtPoly}:
\begin{center}
Sample integer solutions of the integer system
\begin{equation}
\label{transprob}
b + Q_2 w \geq 0
\end{equation}
\end{center}

We now describe the Hermite Normal Form (HNF) algorithm.
This is a simplified version of the algorithm in \cite{C_CCANT_00} adapted to network matrices.
In particular, the requirement for the Hermite normal form that $ B[i,j] < B[i,i] $ for $ j < i $ is not really useful for our purpose so we dropped that part in the algorithm.

\begin{algorithm}[tb]
\caption{HNF($A$,$b$)}
\label{alg:hnf}
\begin{algorithmic}
\REQUIRE An integer $ m \times n $ matrix ($ m \leq n$) of full rank.
\ENSURE
    An integer $ n \times n $ matrix $ Q $ \\
    An integer $ m \times n $ matrix $ B = [B_1|0] $ where $ B_1 $ is $ m \times m $ lower triangular. \\
--------------------------------------------------
    \STATE {\bf (1)} Initialize $ Q \leftarrow I_n $ (the $ n \times n $ identity matrix) and $ B \leftarrow A $
    \FOR{row $ i = 1 $ to $ m $ do:}
        \STATE {\bf (2)} If necessary permutate columns $ i $ and column $ k $ for some $ k > i $ so that $ B[i,i] \not = 0 $ and record the permutation in $ Q $ (if $ B[i,k] = 0 $ for all $ k \geq i $ the rank of $ A $ was not full.)
        \STATE {\bf (3)} If necessary multiply columns $ B[,i] $ and $ Q[,i] $ by $ -1 $ to make $ B[i,i] = 1 $.
        \FOR{each column $ j = i+1 $ to $ n $ such that $ B[i,j] \not = 0 $ do:}
            \IF{$ B[i,j] \not = 0 $}
                \STATE {\bf (4)} replace column $ B[,j] $ by $ B[,j] - B[i,j] B[,i] $.  Record the transformation in $ Q $.
            \ENDIF
        \ENDFOR
    \ENDFOR
\end{algorithmic}
\end{algorithm}

\section{Computational Results on the Example}
\label{sec:uninf_results}
We continue the example introduced in section \ref{ssec:intro_exple}.
The given data consists in the matrix $ A $ shown in \ref{matrix_A} and the vector of observed link counts 
\[
y = \left(\begin{array}{c}
12053370 \\ 14424000 \\ 249121410 \\ 1485729 \\ 248498220 \\ 2365508 \\ 15357294
\end{array}\right)
\]






\vspace{2cm}
\begin{center}
CHANGE FROM HERE ON
\end{center}
\vspace{2cm}










The HNF algorithm of section \ref{sec:main-algo} produces:
\begin{itemize}
\item The permutation of the rows of $ Q $ (or columns of $ A $): \\
$ (1,2,8,9,5,13,7,6,3,10,11,12,4,14,15,16) $
\item The matrices $ Q_{11} $ and $ Q_{12} $
\[
\hspace{-2em}\begin{array}{rl}
Q_{11} =&\!\!\!\! \scriptsize\left(\begin{array}{rrrrrrr}
1 &   &   &   & 1 & -1&   \\
  &   &   &   & -1& 1 &   \\
  &   &   &   &   &   & -1\\
  &   & 1 &   &   &   &   \\
  & 1 &   &   &   & 1 &   \\
  &   &   & 1 &   &   &   \\
  &   &   &   &   & -1& 1 
\end{array}\right) \\
\\
Q_{12} =&\!\!\!\! \scriptsize\left(\begin{array}{rrrrrrrrr}
1 & -1& 1 &   &   & -1& 1 &   &   \\
-1&   & -1&   &   &   & -1&   &   \\
  &   &   &   & -1& -1&   &   & -1\\
  &   & -1& -1& -1&   &   &   &   \\
-1& 1 &   & 1 & 1 & 1 &   & 1 & 1 \\
  &   &   &   &   &   & -1& -1& -1\\
  & -1&   & -1&   &   &   & -1&  
\end{array}\right)
\end{array}
\]
\item The matrix $ B_1 $
\[
B_1 = \scriptsize\left(\begin{array}{rrrrrrr}
1 &   &   &   &   &   &   \\
  & 1 &   &   &   &   &   \\
  &   & 1 &   &   &   &   \\
  &   &   & 1 &   &   &   \\
1 & 1 & 1 & 1 & 1 &   &   \\
  &   &   &   & -1& 1 &   \\
  &   &   &   &   & -1& 1
\end{array}\right)
\]
\end{itemize}
Next we solve the system $ B_1\,x'_1 = y $ and get the vector $ b = Q_{11}x'_1 $
\[
x_1 = \left(\begin{array}{c} 12053370 \\ 14424000 \\ 249121410 \\ 1485729 \\ -28586289 \\ -26220781 \\ -10863487 \end{array}\right)\qquad
b = \left(\begin{array}{c} 9687862 \\ 2365508 \\ 10863487 \\ 249121410 \\ -11796781 \\ 1485729 \\ 15357294 \end{array}\right)
\]
And from there on, we use the function SampleSol($Q_{12},b$) to generate samples of solutions, like:
\[
\left(\begin{array}{c}
4699903 \\ 193924 \\  2073216 \\  5086327 \\  7058077 \\   687677 \\  5405147 \\  1273099 \\ 236524405 \\  1033162 \\ 7605036 \\  3958807 \\   215835 \\   450745 \\   273895 \\   545254 \end{array}\right)
\quad\mbox{or}\quad
\left(\begin{array}{c}
6386434 \\   316730 \\  1270186 \\  4080020 \\  7497193 \\   278638 \\  5518334 \\  1129835 \\ 234466383 \\ 854497 \\ 8436183 \\  5364347 \\   148210 \\   915643 \\   132591 \\   289285 \end{array}\right)
\]

We ran the algorithm on a cluster of machines to produce $ 10 $ independent chains of $ 100,000 $ solutions with a CPU time of about $ 9 $ hours.
The histograms of the coordinates are plotted on figure (\ref{uninfmarginals}).
Notice that the actual solution is, for most of its coordinates, away from the averages.
This means that the actual solution is situated somewhere on the boundary of the polytope.

\begin{figure}[!h]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[width=\columnwidth]{bhaas_1router_t5_uniform.pdf}}
\caption{Histograms of the OD solutions coordinates in the uninformative case.  The black triangles on the x-axis point to the coordinates of the actual solution from the data set.\label{uninfmarginals}}
\end{center}
\vskip -0.2in
\end{figure} 

The sampled solutions are exact in the sense that $ A $ times a solution gives exactly the observation.
The average distance between the sampled solutions and the actual solution is of the order:
\begin{itemize}
\item $L_1$-distance: $ 55 $ Million counts.
\item $L_2$-distance: $ 20 $ Million counts.
\end{itemize}









\bibliography{bibtex/bertrand_references.bib}
\bibliographystyle{icml2010}

\end{document}
