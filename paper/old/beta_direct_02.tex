\documentclass{article}

\usepackage{amsthm,amsmath,amssymb}
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage{natbib}
\usepackage{authblk}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{hyperref}
\newcommand{\theHalgorithm}{\arabic{algorithm}}
%\usepackage{icml2010}
% \usepackage[accepted]{icml2010}


\title{A Beta-Sampler for Point Sampling in Polyhedrons}
\author{Bertrand Haas}
\author{Edoardo Airoldi}
\affil{Harvad University, Department of Statistics}

\theoremstyle{plain}
\newtheorem{thm}{Theorem}[section]
\newtheorem{lem}{Lemma}
\newtheorem{cor}[thm]{Corollary}
\newtheorem{prop}[thm]{Proposition}

\newcommand{\Vol}{\mbox{Vol}}

\graphicspath{{./figures/}}
\begin{document}
\maketitle
\begin{abstract}
Many problems in applied mathematics are set in a domain with constraints given by linear inequalities, that is, a {\em polyhedral domain}, or simply a {\em polyhedron}.
Being able to quickly sample points from a polyhedron is very useful in the process of solving these problems.
Most known algorithms for doing so use MCMC or Metropolis Hating methods.
These methods certainly have many advantages, in particular they scale relatively well with the dimension of the problem, but they have one major disadvantage:  They are sequential.
So when we need a large number of samples, they can be dauntingly slow.
Ideally we would like to have a method that samples points independently from each others and be able to sample them all at once so as to parallelize the problem as much as we can.
We present here a non-sequential method that goes in this direction.
\end{abstract}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
Let $ P $ be a polyhedron defined either by the set of $ x \in \mathbb{R}^n $ satisfying
\begin{equation}
\label{eq:polyeq}
Ax = b, \quad x \geq 0
\end{equation}
where $ A $ is a full rank $ m $ by $ n $ matrix, $ m < n $ (so $ P $ has dimension $ d = n - m $) and $ b \in \mathbb{R}^m $, or by the set of $ x' \in \mathbb{R}^d $ satisfying
\begin{equation}
\label{eq:polyineq}
A'x' \leq b', \quad x \geq 0
\end{equation}
where $ A' $ is a $ m $ by $ d $ full rank matrix and $ b' \in \mathbb{R}^m $.

\subsection{The basic algorithm}
\label{sec:basicalgo}
We propose the following simple coordinate-by-coordinate method (algorithm \ref{algo:samplept00}) for uniformly sampling points in $ P $.
However, we emphasize immediately that the difficult part in this algorithm is to get a good approximation for the marginal distribution along a coordinate (line \ref{samplept00:margapprox}).
This part will comprise the core of this paper.

\begin{algorithm}[tb]
\caption{Sample\_00($A'$, $b'$)}
\label{algo:samplept00}
\begin{algorithmic}[1]
\IF {$ b' = \emptyset $} \label{samplept00:endrecur}
    \STATE return $ \emptyset $
\ENDIF
\STATE Let $ i $ be a (uniformly) random coordinate index among $ \{1, \dots, d\} $
\STATE \textbf{Get an approximation $ f(x) $ of the marginal distribution for coordinate $ i $} \label{samplept00:margapprox}
\STATE Sample $ x[i] $ from $ f(x) $ \label{samplept00:samplexi}
\STATE Let $ A'' $ be the matrix resulting from removing column $ i $ from $ A' $ and possibly removing some rows to make it full rank. \label{samplept00:newA}
\STATE Let $ b'' $ be the result of removing the same rows from $ b' - A'[,i] x[i] $ \label{samplept00:newb}
\STATE Let $ x' = \mbox{Sample\_00}( A'', b'') $ \label{samplept00:recursion}
\STATE Let $ x $ be the result of inserting $ x[i] $ in $ x' $ at the $ i^\mathrm{th} $ position \label{samplept00:xtogether}
\STATE return $ x $
\end{algorithmic}
\end{algorithm}

The geometric intuition behind this algorithm is the following (see figure \ref{fig:3dptopesampling}):
For a polytope $ P_d $ of dimension $ d > 0 $, sample a value $ x[i] = t $ for a random coordinate $ i $ (line \ref{samplept00:samplexi}).
See section \ref{sec:margdist} for a discussion of the sampling distribution.
This defines a section of $ P_d $ that is a polytope $ P_{d-1} $ of dimension $ d-1 $;
more precisely $ P_{d-1} = \{x \in P_d : x[i] = t \} $ (lines \ref{samplept00:newA}--\ref{samplept00:newb}).
Now we recursively iterate (line \ref{samplept00:recursion}) until the dimension of the polytope becomes negative (line \ref{samplept00:endrecur}).
Putting back together the coordinates (line \ref{samplept00:xtogether}) we get the sampled point $ x \in P $.

\begin{figure}[!h]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[width=\columnwidth]{3d_ptope_sampling.png}}
\caption{The sampling algorithm from dimension $3$ on.\label{fig:3dptopesampling}}
\end{center}
\vskip -0.2in
\end{figure} 

\subsection{Motivating applications}
Maximizing a function over a polyhedral domain is a typical applied math problem.
When the function is linear or quadratic there are very efficient methods for doing so (linear programming and quadratic programming).
When the function is more complicated, though, in particular when it has multiple maximums, there is no such apgorithms.
So many algorithms have been designed to find the global maximum with high probability (but with no real guarantee).
They are called metaheuristic algorithms, are often quite efficient, they are iterative and they all rely on an exploration phase of the domain where sampling is of utmost importance.
Typically a batch of points is sampled with a (prior) uniform distribution, the function is evaluated on these points and as we go along a (posterior) distribution is tweaked toward the region where the maximum is believed to be (exploitation phase) for the next exploratory sample batch.

Contingency tables.

Network Tomography.

Frechet bounds?

\section{The marginal distribution along a coordinate}
\label{sec:margdist}
Assume in this section that the polyhedron $ P $ defined by (\ref{eq:polyeq}) or (\ref{eq:polyineq}) is bounded (a polytope).
See section \ref{sec:unbounded} for the unbounded case.
If the distribution on $ P $ is uniform, the marginal distribution along a coordinate is proportional to the $ (d-1) $-volume of the section above ($ d $ being the dimension of $ P $).
But this does not yield a simple distribution.

For simplicity assume we sample the first coordinate $ x[1] $ and let $ f(t) $ be its marginal distribution.
More precisely, let $ x[1] = t_0, t_1, \dots, t_N $ be the values of the first coordinates of all the vertices of $ P $ and let $ S(t) $ be the section of $ P $ at $ x[1] = t $, that is
\[
S(t) = \{x \in P : x[1] = t\}
\]
Let's simply write $ S_i $ for $ S(t_i) $ and let $ P_{i,j} = \{ x \in P : t_i \leq x[1] \leq t_j\} $ be the \emph{slice} of $ P $ between $ S_i $ and $ S_j $.
The following lemma takes advantage of the fact that
\[
f(t) \propto \mathrm{Vol}_{d-1}(S(t))
\]
\begin{lem}
$ f(t) $ is a continuous function, has a unique point or possibly a full interval as mode, and is smooth everywhere except perhaps at values $ t_0, t_1, \dots, t_N $.
\end{lem}
\begin{proof}
Clearly, for $ t_{i-1} \leq t \leq t_i $, the volume $ \mathrm{Vol}_{d-1}(S(t)) $ changes smoothly and its slope at $ t = t_{i-1} $ depends only of $ S_{i-1} $ and $ S_i $.

Moreover, clearly $ \lim_{t \rightarrow t_i, t > 0} f(t) = \lim_{t \rightarrow t_i, t < 0} f(t) = f(t_i) $, so $ f(t) $ is continuous everywhere.

We can construct $ P $ slice by slice:  $ P = P_{0,1} \cup P_{1,2} \dots \cup P_{N-1,N} $.
In constructing slice $ P_{i,i+1} $ we can choose whatever we want for $ S_{i+1} $ as long as it is in the cone defined by the edges linking $ S_{i-1} $ to $ S_i $ (to preserve convexity).
Therefore the right slope $ \frac{d}{dt}_{(t>0)}f(t_i) $ has no reason to be the same as the left slope $ \frac{d}{dt}_{(t<0)}f(t_i) $.

Since $ P $ is convex, the sectional volume has only one maximum, and that maximum is achieved on a connected set (a point or a full interval).
Therefore $ f(t) $ has only one point or possibly a full interval as mode.
\end{proof}
Computing the exact distribution would involve computing the first coordinates of all the vertices, and if we need to do that for all coordinates we need to compute all the vertices.
However, the number of vertices grows exponentially with the size of the problem (of the matrix $ A $ in (\ref{eq:polyeq}) or $ A' $ in (\ref{eq:polyineq})).
For example an elementary polytope like a cube of dimension $ d $ in general position is defined by $ 2d $ hyperplanes, so $ A' $ is $ 2d $ by $ d $, but the number of vertices is $ 2^d $, and because it is in general position there are as many $ t_i $'s to compute.
Therefore computing the exact distribution is not tractable in high dimensions.

Hence, we need to approximate it with a simpler distribution.
Since $ f(t) $ is defined on an interval and is unimodal (even if on a full sub-interval), we choose to approximate it by a scaled beta distribution.
That is, $ X[1] = t_0 + (t_N - t_0)Y $, with $ Y \sim \mathrm{Beta}(a,b) $, and $ a,b \geq 1 $.
This still involves computing $ t_0 $ and $ t_1 $, and we discuss this in section \ref{}.

With $ s = (t- t_0)/t_N - t_0) $, the approximate pdf for $ Y $ is therefore $ f(s) \propto s^{a-1}(1-s)^{b-1} $.
Moreover, $ \mathrm{Vol}_{d-1}(S(s)) $ is a polynomial in $ s $ of degree $ d-1 $.
For example if $ S_0 = 0 $ (so the slice $ P_{0,1} $ is a pyramid over $ S_1 $), then $ \mathrm{Vol}_{d-1}(S(s)) = \mathrm{Vol}_{d-1}(S_1) s^{d-1} $.
Therefore we must have $ (a-1) + (b-1) = (d-1) $, that is
\[
a+b = d+1
\]
Which leaves only one degree of freedom for choosing $ a $ and $ b $.
In section \ref{} we discuss several choices for an additional constraint that yields a unique $ a $ and $ b $.

\section{The unbounded case}
\label{sec:unbounded}
In the previous section we assumed that $ P $ was bounded.
Suppose now $ P $ is unbounded along the coordinate of interest, say $ x[1] $.
From the defining equation (\ref{eq:poyeq}) or (\ref{eq:polyineq}), it is clear that $ x[1] $ can be unbounded only on the positive side.
Then the transformation
\[
g : x[1] = t \mapsto r = \frac{t}{(1+t)}
\]
will transform the interval $ [0,\infty] $ to $ [0,1] $, on which we can sample, as in section \ref{sec:margdist}, with a beta distribution.
There are many transformations mapping $ [0, \infty] \rightarrow [0,1] $, but the advantage of this one is that it is a \emph{projective transformation}, and as such it preserves linearity.
So the transformed systems (\ref{eq:polyeq}) or (\ref{eq:polyineq}) remain linear.
Alternatively we can use directly the transformed distribution, known as a \emph{Beta prime} distribution, defined on $ [0, \infty] $ by the random variable
\[
T = g^{-1}(R) = \frac{R}{(1-R)} \quad\mbox{for}\quad R \sim \mathrm{Beta}(a,b)
\]
In this case the pdf of $ T $ is
\[
f(t) = \frac{\Gamma(a+b)}{\Gamma(a)\Gamma(b)} \frac{t^{a-1}}{(1+t)^{a+b}}
\]
If $ d = 1 $ (the dimension of $ P $) and $ P $ is unbounded, then there is no unique distribution on it that generalizes the uniform distribution that occur when $ P $ is bounded.
It is interesting that our choice of projective transformation $ g $ (justified for $ d > 1 $) yields the pdf $ f(t) = (1+t)^{-2} $ on $ (0,+\infty) $ as a natural generalization of the uniform pdf.

\section{Parameters for the Beta distribution}
\label{sec:betaparam}
In section \ref{sec:margdist} we saw that our choice of a beta (or beta-prime) distribution with parameters $ (a,b) $ as an approximate marginal distribution for $ x[1] $, came with the contraint $ a + b = d+1 $, $ a,b \geq 1 $, which leaves only one more degree of freedom in choosing $ a $ and $ b $.
It seems that beside $ t_0 $ and $ t_N $, the extreme values for $ x[1] $, we do not know any directly useful information about $ P $ to set $ a $ and $ b $.
A useful information would certainly be the center of mass $ x_m \in P $ as its $ i^\mathrm{th} $ coordinate would be the mean of the $ i^\mathrm{th} $ marginal pdf.

Finding the exact center of mass, though, is a difficult problem and we have to fall back to some approximation.
The easiest way, conceptually and computationally, is to make do with no information.
In that case the most reasonable guess is the mid-point between $ t_0 $ and $ t_N $;
Indeed the $ x_m $ has no reason to be more left than more right of it.
We' ll do just that in section \ref{}.

Or we can try to gather some more information and get a better guess for the position of $ x_m $.
In section \ref{}, we will discuss the use of the neighbors of the end points, at $ t_0 $ and $ t_N $, to this purpose.

Or we can use an approximating center for the center of mass.
There are many centers defined on a convex sets.
In section \ref{}, we will mention two:
The analytic center, which is relatively inexpensive to compute but turns out to not be a very good approximation to $ x_m $.
The center of the Maximum Volume Inscribed Ellipsoid (\emph{MVIE}) which gives a better approximation but is also more costly to compute.

\section{Finding the minimum and maximum coordinates}
\label{sec:minmaxcoord}
In section \ref{margdist} our approximate beta distribution allows us to by-pass the computation of the first coordinates of all the vertices, except the minimum and maximum coordinate, $ t_0 $ and $ t_N $.
We use here standard techniques from linear programming (\emph{LP}), particularly the simplex method, to get these values.
The LP constraints are given by (\ref{eq:polyeq}) or (\ref{eq:polyineq}), and the function to optimize is $ f(x) = x[1] $.
This function is so simple that we can slightly simplify the simplex method (see \cite{}).

\section{The detailed algorithm}
Putting things together we now describe the detailed version of the algorithm.
Algorithm \ref{algo:samplept01} is the algorithm for sampling one point, and algorithm \ref{algo:samplebunch} is a ``wrapper'' for sampling a bunch of points divided into $ K $ batches of $ L $ points.

\begin{algorithm}[h]
\caption{SamplePoint$(A,b,\hat{c})$}
\label{algo:samplept01}
\begin{algorithmic}[1]
    \STATE Let $ d = n - m $ (the matrix $ A $ is $ m \times n $, with $ m \leq n $)
    \IF {$ d = 0 $}
        \STATE Return the solution of $ Ax = b $ if it's non-negative, otherwise ``infeasible''
    \ELSE
        \STATE Let $ i $ be a random coordinate.
        \STATE Get the minimum and maximum values, respectively $ t_0 $ and $ t_N $, of $ x[i] $.
        \IF {$ \hat{c} = \emptyset $}
            \STATE Let $ \alpha = \beta = \frac{d+1}{2} $
        \ELSE
            \STATE Let $ \alpha = (d + 1) (\hat{c}[i] - t_0)/(t_N - t_0) $ and $ \beta = d + 1 - \alpha $
        \ENDIF
        \IF {$ t_N < \infty $}
            \STATE Sample a value $ T \sim \mathrm{Beta}(\alpha, \beta) $
            \STATE Let $ x[i] = t_0 + T (t_N - t_0) $
        \ELSE
            \STATE Sample a value $ T \sim \mathrm{Beta\_prime}(\alpha, \beta) $
            \STATE Let $ x[i] = t_0 + T $
        \ENDIF
        \STATE Let $ b' = b - A[,i] x[i] $
        \STATE Let $ \hat{c}' $ be the result of removing coordinate $ i $ from $ \hat{c} $.
        \STATE Let $ A' $ be the result of removing column $ i $ from $ A $.
        \IF {$ A' $ is not full rank}
            \STATE Find appropriate redundant rows.
            \STATE Check the redundancy is the same in $ b $ (if not, system is infeasible).
            \STATE Remove the rows in $ A' $ and $ b' $.
        \ENDIF
        \STATE Let $ x' = \mathrm{SamplePoint}(A',b',\hat{c}') $ (if ``infeasible'', return ``infeasible'')
        \STATE Let $ x $ be the result of inserting $ x[i] $ as coordinate $ i $ in $ x' $.
        \STATE Return $ x $.
    \ENDIF
\end{algorithmic}

\end{algorithm}
\begin{algorithm}[h]
\caption{SampleBunch$(A,b,K,L)$}
\label{algo:samplebunch}
\begin{algorithmic}
    \STATE Let $ \hat{c} = \emptyset $
    \STATE Let $ X = \emptyset $
    \FOR {$ k = 1 $ to $ K $}
        \FOR {$ \ell = 1 $ to $ L $}
            \STATE Let $ x = \mathrm{SamplePoint}(A,b,\hat{c}) $
            \STATE Let $ X = X \cup \{x\} $
        \ENDFOR
        \STATE Let $ \hat{c} = \mathrm{mean}(x \in X) $
    \ENDFOR
    \STATE Return $ X $
\end{algorithmic}
\end{algorithm}

\section{Experimental results}
\label{sec:expresults}
We apply our method to some data taken from a simple problem in network tomography from \cite{CDWY_TVNT_00}.
This is a small network with $ 4 $ Origin-Destination (or \emph{OD}) nodes ($a$, $b$, $c$, and $d$) and one router ($r$) (see figure \ref{sple_netw_diagr}).
The traffic (in bytes) is known on each edge and we want to infer the traffic between all the OD-nodes.
There are $ 4^2 $ pairs of nodes, so $ x $ is $ 16 $-dimensional, and we order its coordinates lexicographically, that is, $ x[1] = aa $, $ x[2] = ab, \dots, x[5] = ba $, $ x[6] = bb, \dots $, where $ aa $ is the traffic between OD-node $ a $ and itself (we allow self-communication), $ ab $ between $ a $ and $ b $, etc..

\begin{figure}[!h]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[scale=.6]{sple_netw_diagr.png}}
\caption{a simple network.\label{sple_netw_diagr}}
\end{center}
\vskip -0.2in
\end{figure} 

The constraints are represented by the traffic on the edges.
There are $ 8 $ of them, but since the incoming traffic at the router equals the outgoing traffic, we can get one from the rest (and no more).
Therefore $ b $ is $ 7 $-dimensional, and we order its coordinate lexicographically, $ b[1] = ar$, $ b[2] = br, \dots, b[7] = rc $, where $ ar $ is the traffic between OD-node $ a $ and the router, etc..
Hence, the marix from the defining system (\ref{eq:polyeq}), $ Ax = b $, $ x \geq 0 $ is:
\begin{equation}
\label{matrixA}
%\tiny
\hspace{-1em}A = \left(\begin{array}{cccccccccccccccc}
1  & 1  & 1  & 1  &    &    &    &    &    &    &    &    &    &    &    &    \\
&    &    &    & 1  & 1  & 1  & 1  &    &    &    &    &    &    &    &    \\
&    &    &    &    &    &    &    & 1  & 1  & 1  & 1  &    &    &    &    \\
&    &    &    &    &    &    &    &    &    &    &    & 1  & 1  & 1  & 1  \\
1  &    &    &    & 1  &    &    &    & 1  &    &    &    & 1  &    &    &    \\
& 1  &    &    &    & 1  &    &    &    & 1  &    &    &    & 1  &    &    \\
&    & 1  &    &    &    & 1  &    &    &    & 1  &    &    &    & 1  &    
\end{array}\right)
\end{equation}
where the zero entries are left blank for clarity.
Notice that this is exactly the same set up as for a $ 4 $ by $ 4 $ contingency table.
From the data set in \cite{CDWY_TVNT_00} we randomly chose a vector $ b $ (the $ 5^\mathrm{th} $ one in the dataset):
\[
b = \left(\begin{array}{c}
12053370 \\ 14424000 \\ 249121410 \\ 1485729 \\ 248498220 \\ 2365508 \\ 15357294
\end{array}\right)
\]

\subsection{The exact distribution}
We computed the vertices of the polytope $ P $ with the R-package $ \texttt{rcdd} $ (\cite{rcdd}).
There are $ 232 $ vertices.








\bibliography{bibtex/1.bertrand_references}
\bibliographystyle{plain}

\end{document}
