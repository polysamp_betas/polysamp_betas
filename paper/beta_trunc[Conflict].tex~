\documentclass{article}

\usepackage{amsthm,amsmath,amssymb}
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage{natbib}
\usepackage{authblk}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{hyperref}
\newcommand{\theHalgorithm}{\arabic{algorithm}}
%\usepackage{icml2010}
% \usepackage[accepted]{icml2010}


\title{A Beta-Sampler for Point Sampling in Polyhedrons}
\author{Bertrand Haas}
\author{Edoardo Airoldi}
\affil{Harvad University, Department of Statistics}

\theoremstyle{plain}
\newtheorem{thm}{Theorem}[section]
\newtheorem{lem}{Lemma}
\newtheorem{cor}[thm]{Corollary}
\newtheorem{prop}[thm]{Proposition}

\newcommand{\Vol}{\mbox{Vol}}

\graphicspath{{./figures/}}
\begin{document}
\maketitle
\begin{abstract}
Many problems in applied mathematics are set in a domain with constraints given by linear inequalities, that is, a {\em polyhedral domain}, or simply a {\em polyhedron}.
Being able to quickly sample points from a polyhedron is very useful in the process of solving these problems.
Most known algorithms for doing so use MCMC or Metropolis Hating methods.
These methods certainly have many advantages, in particular they scale relatively well with the dimension of the problem, but they have one major disadvantage:  They are sequential.
So when we need a large number of samples, they can be dauntingly slow.
Ideally we would like to have a method that samples points independently from each others and be able to sample them all at once so as to parallelize the problem as much as we can.
We present here a non-sequential method that goes in this direction.
\end{abstract}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
\subsection{Setting and results}
Let $ P $ be a polyhedron defined either by the set of $ x \in \mathbb{R}^n $ satisfying
\begin{equation}
\label{eq:polyeq}
Ax = b, \quad x \geq 0
\end{equation}
where $ A $ is a full rank $ m $ by $ n $ matrix, $ m < n $ (so $ P $ has dimension $ d = n - m $) and $ b \in \mathbb{R}^m $, or by the set of $ x' \in \mathbb{R}^d $ satisfying
\begin{equation}
\label{eq:polyineq}
A'x' \leq b', \quad x \geq 0
\end{equation}
where $ A' $ is a $ m $ by $ d $ full rank matrix and $ b' \in \mathbb{R}^m $.
\label{sec:introres}
We propose the following simple coordinate-by-coordinate method (algorithm \ref{algo:samplept00}) for uniformly sampling points in $ P $.
However, we emphasize immediately that the difficult part in this algorithm is to get a good approximation for the marginal distribution along a coordinate (line \ref{samplept00:margapprox}).
This part will comprise the core of this paper.

\begin{algorithm}[tb]
\caption{Sample\_00($A$, $b$)}
\label{algo:samplept00}
\begin{algorithmic}[1]
\STATE Simplify $ (A,b) $ to $ (A', b') $ by removing some rows from $ (A, b) $ if necessary to make $ A' $ full rank. (See also section \ref{sec:geom} for further simplifications)\label{samplept00:newA}
\IF {$ A $ is a square matrix} \label{samplept00:endrecur}
    \STATE Solve $ Ax = b $
    \STATE return $ x $ (or "infeasible" if $ x $ has some negative coordinates)
\ENDIF
\STATE Let $ i $ be a (uniformly) random coordinate index among $ \{1, \dots, n\} $
\STATE \textbf{Get an approximation $ f(x) $ of the marginal distribution for coordinate $ i $} \label{samplept00:margapprox}
\STATE Sample $ x[i] $ from $ f(x) $ \label{samplept00:samplexi}
\STATE Let $ b'' = b' - x[i] A'[,i] $
\STATE Let $ A'' $ be the result of removing column $ i $ from $ A' $
\STATE Let $ x'' = \mbox{Sample\_00}( A'', b'') $ (if feasible, otherwise return "infeasible") \label{samplept00:recursion}
\STATE Let $ x $ be the result of inserting $ x[i] $ in $ x'' $ at the $ i^\mathrm{th} $ position \label{samplept00:xtogether}
\STATE return $ x $ \label{samplept00:return}
\end{algorithmic}
\end{algorithm}

The algorithm's geometric interpretation is the following (see figure \ref{fig:3dptopesampling}):
For a polytope $ P_d $ of dimension $ d > 0 $, sample a value $ x[i] = t $ for a random coordinate $ i $ (line \ref{samplept00:samplexi}).
See section \ref{sec:margdist} for a discussion of the sampling distribution.
This defines a section of $ P_d $ that is a polytope $ P_{d-1} $ of dimension $ d-1 $;
more precisely $ P_{d-1} = \{x \in P_d : x[i] = t \} $ (lines \ref{samplept00:newA}--\ref{samplept00:newb}).
Now we recursively iterate (line \ref{samplept00:recursion}) until the dimension of the polytope becomes negative (line \ref{samplept00:endrecur}).
Putting back together the coordinates (line \ref{samplept00:xtogether}) we get the sampled point $ x \in P $.

\begin{figure}[!h]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[width=\columnwidth]{3d_ptope_sampling.png}}
\caption{The sampling algorithm from dimension $3$ on.\label{fig:3dptopesampling}}
\end{center}
\vskip -0.2in
\end{figure} 

\subsection{Motivations}
Maximizing a function over a polyhedral domain is a typical applied math problem.
When the function is linear or quadratic there are very efficient methods for doing so (linear programming and quadratic programming).
When the function is more complicated, though, in particular when it has multiple maximums, there is no such apgorithms.
So, many algorithms have been designed to find the global maximum with high probability (but with no real guarantee).
They are called metaheuristic algorithms, are often quite efficient, they are iterative and they all rely on an exploration phase of the domain during which sampling is of utmost importance.
Typically a batch of points is sampled with a (prior) uniform distribution, the function is evaluated on these points and as we go along a (posterior) distribution is tweaked toward the region where the maximum is believed to be (exploitation phase) for the next exploratory sample batch.

Contingency tables.

Network Tomography.

Frechet bounds?

\section{Geometry of the problem}
\label{sec:geom}
The facets of the polyhedron $ P $ defined by (\ref{eq:polyeq}) are the intersections of hyperplanes of coordinates $ H_i = \{x : x[i] = 0\} $ and the affine space defined by $ Ax = b $.
However, not all hyperplanes $ H_i $ support a facet.
If $ H_j $ does not support a facet, then it is redundant in defining $ P $, and we can remove it, that is, we can remove $ x[j] $ and column $ A[j] $ from problem (\ref{eq:polyeq}).
See line \ref{samplept00:newA} from algorithm \ref{algo:samplept00}.
Of course, at the end, coordinate $ x[j] $ might be of as much interest as the other ones, and one needs to recover it.
But since $ n > m $, it is a linear combination of the other coordinates.
So we just have to keep such a linear combination in memory and use it between line \ref{samplept00:xtogether} and \ref{samplept00:return} in algorithm \ref{algo:samplept00} to recover $ x[j] $.
The following proposition gives a bound on the number of redundant hyperplanes.

\begin{prop}
The number of redundant hyperplanes is at most $ m $, the number of rows of $ A $ in (\ref{eq:polyeq}).
\end{prop}

\begin{proof}
Since $ A $ in (\ref{eq:polyeq}) is an $ m \times n $ (with $ n \geq m $) full rank matrix, the dimension of the affine space $ \mathcal{S} = \{x: Ax = b\} $ (and therefore of $ P $, its intersection with the positive orthant $ \mathbb{R}^n_{\geq 0} $) is $ d = n-m $.
Since $ \mathbb{R}^n_{\geq 0} $ is a cone with a vertex (the origin $ 0 $ is the only vertex), it does not contain a line.
Therefore $ P $ does nor contain a line either, so it has at least one vertex.
Such a vertex is the intersection of at least $ d $ hyperplanes in $ \mathcal{S} $.
So there is at least $ d $ hyperplanes $ H_i $ supporting facets of $ P $.
In other words there is at most $ n - d = n - (n-m) = m $ redundant hyperplanes.
\end{proof}

\begin{cor}
The number of coordinates $ x[i] $ such that $ \min(x[i]) > 0 $ over all $ x \in P $ is at most $ m $.
\end{cor}

\begin{proof}
If $ H_i $ does support a facet, then the minimum value for $ x[i] $ is clearly $ 0 $.
Conversely, if the minimum of $ x[i] $ is positive, then $ H_i $ does not support a facet of $ P $ and is redundant.
So the number of coordinates $ x[i] $ such that $ \min(x[i]) > 0 $ is at most equal to the number of redundant hyperplanes.
\end{proof}














\section{The marginal distribution along a coordinate}
\label{sec:margdist}
Assume in this section that the polyhedron $ P $ defined by (\ref{eq:polyeq}) or (\ref{eq:polyineq}) is bounded (a polytope).
See section \ref{sec:unbounded} for the unbounded case.
If the distribution on $ P $ is uniform, the marginal distribution along a coordinate is proportional to the $ (d-1) $-volume of the section above ($ d $ being the dimension of $ P $).
But this does not yield a simple distribution.

For simplicity assume we sample the first coordinate $ x[1] $ and let $ f(t) $ be its marginal distribution.
More precisely, let $ x[1] = t_0, t_1, \dots, t_N $ be the different values of the first coordinates of all the vertices of $ P $.
Let $ S(t) $ be the section of $ P $ at $ x[1] = t $, that is
\[
S(t) = \{x \in P : x[1] = t\}
\]
Let's simply write $ S_i $ for $ S(t_i) $ and let $ P_{i,j} = \{ x \in P : t_i \leq x[1] \leq t_j\} $ be the \emph{slice} of $ P $ between $ S_i $ and $ S_j $.
The following lemma takes advantage of the fact that
\[
f(t) \propto \mathrm{Vol}_{d-1}(S(t))
\]
\begin{lem}
$ f(t) $ is a continuous function, has a unique point or possibly a full interval as mode, and is smooth everywhere except perhaps at the values $ t_0, t_1, \dots, t_N $.
\end{lem}
\begin{proof}
Clearly, for $ t_{i-1} \leq t \leq t_i $, the volume $ \mathrm{Vol}_{d-1}(S(t)) $ changes smoothly and its slope at $ t = t_{i-1} $ depends only of $ S_{i-1} $ and $ S_i $.

Moreover, clearly $ \lim_{t \rightarrow t_i, t > 0} f(t) = \lim_{t \rightarrow t_i, t < 0} f(t) = f(t_i) $, so $ f(t) $ is continuous everywhere.

We can construct $ P $ slice by slice:  $ P = P_{0,1} \cup P_{1,2} \dots \cup P_{N-1,N} $.
In constructing slice $ P_{i,i+1} $ we can choose whatever we want for $ S_{i+1} $ as long as it is in the cone defined by the edges linking $ S_{i-1} $ to $ S_i $ (to preserve convexity).
Therefore the right slope $ \frac{d}{dt}_{(t>0)}f(t_i) $ has no reason to be the same as the left slope $ \frac{d}{dt}_{(t<0)}f(t_i) $.

Since $ P $ is convex, the sectional volume has only one maximum, and that maximum is achieved on a connected set (a point or a full interval).
Therefore $ f(t) $ has only one point or possibly a full interval as mode.
\end{proof}
Computing the exact distribution would involve computing the first coordinates of all the vertices, and if we need to do that for all coordinates we need to compute all the vertices.
However, the number of vertices grows exponentially with the size of the problem (of the matrix $ A $ in (\ref{eq:polyeq}) or $ A' $ in (\ref{eq:polyineq})).
For example an elementary polytope like a cube of dimension $ d $ in general position is defined by $ 2d $ hyperplanes, so $ A' $ is $ 2d $ by $ d $, but the number of vertices is $ 2^d $, and because it is in general position there are as many $ t_i $'s to compute.
Therefore computing the exact distribution is not tractable in high dimensions.

Since $ f(t) $ is defined on an interval and is unimodal (even if on a full sub-interval), we choose to approximate it by a scaled beta distribution.
That is, $ X[1] = t_0 + (t_N - t_0)Y $, with $ Y \sim \mathrm{Beta}(a,b) $, and $ a,b \geq 1 $.
This still involves computing $ t_0 $, $ t_N $, $a $ and $b$, and we discuss this in section \ref{sec:betaparam}.

With $ s = (t- t_0)/(t_N - t_0) $, the approximate pdf for $ Y $ is therefore $ f(s) \propto s^{a-1}(1-s)^{b-1} $.
Moreover, $ \mathrm{Vol}_{d-1}(S(s)) $ is a polynomial in $ s $ of degree $ d-1 $.
For example if $ S_0 = 0 $ (so the slice $ P_{0,1} $ is a pyramid over $ S_1 $), then $ \mathrm{Vol}_{d-1}(S(s)) = \mathrm{Vol}_{d-1}(S_1) s^{d-1} $.
Therefore we must have $ (a-1) + (b-1) = (d-1) $, that is
\[
a+b = d+1
\]
Which leaves only one degree of freedom for choosing $ a $ and $ b $.
In section \ref{} we discuss several choices for an additional constraint that yields a unique $ a $ and $ b $.

\section{The unbounded case}
\label{sec:unbounded}
In the previous section we assumed that $ P $ was bounded.
Suppose now $ P $ is unbounded along the coordinate of interest, say $ x[1] $.
From the defining equation (\ref{eq:polyeq}) or (\ref{eq:polyineq}), it is clear that $ x[1] $ can be unbounded only on the positive side.
Then the transformation
\[
g : x[1] = t \mapsto r = \frac{t}{(1+t)}
\]
will transform the interval $ [0,\infty] $ to $ [0,1] $, on which we can sample, as in section \ref{sec:margdist}, with a beta distribution.
There are many transformations mapping $ [0, \infty] \rightarrow [0,1] $, but the advantage of this one is that it is a \emph{projective transformation}, and as such it preserves linearity.
So the transformed systems (\ref{eq:polyeq}) or (\ref{eq:polyineq}) remain linear.
Alternatively we can use directly the transformed distribution, known as a \emph{Beta prime} distribution, defined on $ [0, \infty] $ by the random variable
\[
T = g^{-1}(R) = \frac{R}{(1-R)} \quad\mbox{for}\quad R \sim \mathrm{Beta}(a,b)
\]
In this case the pdf of $ T $ is
\[
f(t) = \frac{\Gamma(a+b)}{\Gamma(a)\Gamma(b)} \frac{t^{a-1}}{(1+t)^{a+b}}
\]
If $ d = 1 $ (the dimension of $ P $) and $ P $ is unbounded, then there is no unique distribution on it that generalizes the uniform distribution that occur when $ P $ is bounded.
It is interesting that our choice of projective transformation $ g $ (justified for $ d > 1 $) yields the pdf $ f(t) = (1+t)^{-2} $ on $ (0,+\infty) $ as a natural generalization of the uniform pdf.

\section{Parameters for the Beta distribution}
\label{sec:betaparam}
In section \ref{sec:margdist} we saw that our choice of a beta (or beta-prime) distribution with parameters $ (a,b) $ as an approximate marginal distribution for $ x[1] $, came with the constraint $ a + b = d+1 $, $ a,b \geq 1 $, which leaves only one more degree of freedom in choosing $ a $ and $ b $.
It seems that beside $ t_0 $ and $ t_N $, the extreme values for $ x[1] $, we do not know any directly useful information about $ P $ to set $ a $ and $ b $.

In our experiments we used several ways to set $ a $ and $ b $.






A useful information would certainly be the center of mass $ x_m \in P $ as its $ i^\mathrm{th} $ coordinate would be the mean of the $ i^\mathrm{th} $ marginal pdf.

Finding the exact center of mass, though, is a difficult problem and we have to fall back to some approximation.
The easiest way, conceptually and computationally, is to make do with no information.
In that case the most reasonable guess is the mid-point between $ t_0 $ and $ t_N $;
Indeed the $ x_m $ has no reason to be more left than more right of it.
We' ll do just that in section \ref{}.

Or we can try to gather some more information and get a better guess for the position of $ x_m $.
In section \ref{}, we will discuss the use of the neighbors of the end points, at $ t_0 $ and $ t_N $, to this purpose.

Or we can use an approximating center for the center of mass.
There are many centers defined on a convex sets.
In section \ref{}, we will mention two:
The analytic center, which is relatively inexpensive to compute but turns out to not be a very good approximation to $ x_m $.
The center of the Maximum Volume Inscribed Ellipsoid (\emph{MVIE}) which gives a better approximation but is also more costly to compute.

\subsection{Finding the minimum and maximum coordinates}
In section \ref{sec:margdist} our approximate beta distribution allows us to by-pass the computation of the first coordinates of all the vertices, except the minimum and maximum coordinate, $ t_0 $ and $ t_N $.
We use here standard techniques from linear programming (\emph{LP}), particularly the simplex method, to get these values.
The LP constraints are given by (\ref{eq:polyeq}) or (\ref{eq:polyineq}), and the function to optimize is $ f(x) = x[1] $.
This function is so simple that we can slightly simplify the simplex method (see \cite{}).

\subsection{A choice of measure for goodness of fit}
In most system (like for contingency tables) the quantities of interest are the original variables, their distributions and their dependences.
This is why we decided to focus on the marginal distributions for each coordinate.
Since each coordinate marginal is one-dimensional, it makes it easy to use well-known 1-dimensional goodness of fit tests.
Unless we are interested in a specific variable in a specific problem, it makes sense to treat all variables equally.
Therefore we found that the mean goodness of fit statistic over all variables is an appropriate goodness of fit measure for the distribution over $ P $.

\subsection{The geometry of the system}
The facets of 


\section{Experimental results}
\label{sec:expresults}
We apply our method to some data taken from a simple problem in network tomography from \cite{CDWY_TVNT_00}.
This is a small network with $ 4 $ Origin-Destination (or \emph{OD}) nodes ($a$, $b$, $c$, and $d$) and one router ($r$) (see figure \ref{sple_netw_diagr}).
The traffic (in bytes) is known on each edge and we want to infer the traffic between all the OD-nodes.
There are $ 4^2 $ pairs of nodes, so $ x $ is $ 16 $-dimensional, and we order its coordinates lexicographically, that is, $ x[1] = aa $, $ x[2] = ab, \dots, x[5] = ba $, $ x[6] = bb, \dots $, where $ aa $ is the traffic between OD-node $ a $ and itself (we allow self-communication), $ ab $ between $ a $ and $ b $, etc..

\begin{figure}[!h]
\vskip 0.2in
\begin{center}
\centerline{\includegraphics[scale=.6]{sple_netw_diagr.png}}
\caption{a simple network.\label{sple_netw_diagr}}
\end{center}
\vskip -0.2in
\end{figure} 

The constraints are represented by the traffic on the edges.
There are $ 8 $ of them, but since the incoming traffic at the router equals the outgoing traffic, we can get one from the rest (and no more).
Therefore $ b $ is $ 7 $-dimensional, and we order its coordinate lexicographically, $ b[1] = ar$, $ b[2] = br, \dots, b[7] = rc $, where $ ar $ is the traffic between OD-node $ a $ and the router, etc..
Hence, the marix from the defining system (\ref{eq:polyeq}), $ Ax = b $, $ x \geq 0 $ is:
\begin{equation}
\label{matrixA}
%\tiny
\hspace{-1em}A = \left(\begin{array}{cccccccccccccccc}
1  & 1  & 1  & 1  &    &    &    &    &    &    &    &    &    &    &    &    \\
&    &    &    & 1  & 1  & 1  & 1  &    &    &    &    &    &    &    &    \\
&    &    &    &    &    &    &    & 1  & 1  & 1  & 1  &    &    &    &    \\
&    &    &    &    &    &    &    &    &    &    &    & 1  & 1  & 1  & 1  \\
1  &    &    &    & 1  &    &    &    & 1  &    &    &    & 1  &    &    &    \\
& 1  &    &    &    & 1  &    &    &    & 1  &    &    &    & 1  &    &    \\
&    & 1  &    &    &    & 1  &    &    &    & 1  &    &    &    & 1  &    
\end{array}\right)
\end{equation}
where the zero entries are left blank for clarity.
Notice that this is exactly the same set up as for a $ 4 $ by $ 4 $ contingency table.
From the data set in \cite{CDWY_TVNT_00} we randomly chose a vector $ b $ (the $ 5^\mathrm{th} $ one in the dataset):
\[
b = \left(\begin{array}{c}
12053370 \\ 14424000 \\ 249121410 \\ 1485729 \\ 248498220 \\ 2365508 \\ 15357294
\end{array}\right)
\]

\subsection{The exact distribution}
This example is small enough that we can compute the exact uniform distribution on $ P $ by the triangulation method described in \ref{Airoldi2011pap}.
More precisely, The polytope $ P $ is represented by the defining system (\ref{eq:polyeq}) as a \emph{polyhedron}, that is, as the intersection of $ n $ half-spaces.
This is called its $ H $-representation (see \cite{Fukuda2004lk}).
Since $ P $ is bounded as a polyhedron, it can aslo be represented as the convex hull of its vertices (a \emph{polytope}), that is, its $ V $-representation.
The R-package $ \texttt{rcdd} $ (\cite{rcdd}) computes the $ V $-representation of $ P $ from its $ H $-representation and vice-versa.
So we used it to compute the vertices (there are $ 232 $ of them).

We projected the vertices on a hyperplane of coordinates of same dimension $ d $ as $ P $.
We then lifted the vertices by selecting a random new coordinate in an additional dimension.
Because the additional $ (d+1)^\mathrm{th} $ coordinates were chosen at random, the convex hull of the lifted set of vertices is a \emph{simplicial} polytope $ P' $, that is, all its facets are simplices.
The convex hull of the lifted set of vertices is a polytope $ P' $ of dimension $ d+1 $.
Then we used again the R-package $ \texttt{rcdd} $ to compute the $ H $-representation of $ P' $.
This gives us all the hyperplanes supporting the facets of $ P' $.
We consider only the hyperplanes facing downward (with a negative $ (d+1) $-coordinate).
For each such hyperplane, it is easy to gather the $ d+1 $ lifted vertices of its supported facet (it is a simplex).

We now project back to $ d $ dimension by forgetting the added $ (d+1)^\mathrm{th} $ coordinate.
Each $ (d+1) $-tuplets of vertices corresponds now to a simplex $ \Sigma_i $ in $ P $.
Because these simplices come from a projection of the lower facets of $ P' $ they decompose $ P $ into a triangulation $ \Sigma = \{\Sigma_i, i=1,\dots,N\} $.
This is a well-known trick (see for example \ref{Deloera2010bk}) to find a triangulation.
Such a triangulation is called a \emph{convex} triangulation.
In our example we got a triangulation with $ 40263 $ simplices on one run and $ 37233 $ on another run (it is normal for different triangulations of the same poytope to have different numbers of simplexes).

It is now easy to compute the $ d $-volume $ V_i $ of each simplex $ \Sigma_i $.
The exact sampling algorithm is now straightforward (algorithm \ref{algo:exactsampler}).

\begin{algorithm}[h]
\caption{ExactSample$(\Sigma)$}
\label{algo:exactsampler}
\begin{algorithmic}[1]
    \STATE Compute the volumes $ V_1, \dots, V_N $ of all the simplices of $ \Sigma $.
    \STATE Compute the total volume $ V_0 = \sum V_i $ of $ P $.
    \STATE Draw a simplex $ \Sigma_i $ with probability $ V_i/V_0 $.
    \STATE Draw a point $ p_i' $ from a Dirichlet distribution with parameters $ (1,\dots,1) $ (uniform on the standard simplex).
    \STATE Scale $ p_i' $ to a point $ p_i $ on $ \Sigma_i $.
    \STATE Return $ p_i $.
\end{algorithmic}
\end{algorithm}

Once again we stress that this exact sampler works well here because the dimensions of the problem is small enough (yet high enough to be non-trivial).
On higher dimension problem, though, it would be intractable since the number of vertices gows exponentially and the number of simplices gows doubly exponentially.
This is why we are developing algorithm, which scales easily in all dimensions, though is not exact.
We can now compare the distribution achieved by algorithm \ref{algo:samplept01} to the exact uniform distribution on $ P $.









The Kolmogorov-Smirnov test usually requires the knowledge of the cdf for the true distribution and we don't know its analytic form.
Therefore we decided for the Pearson's Chi-squared test.
We selected two coordinates of interest, $ x[1] $ and $ x[9] $ (we'll see why in section \ref{sec:}).
We regularly partitioned the intervals $ [t_0, t_N] $ for a selected coordinate into a $ 1000 $ bins.
We then simulated a sampling from the exact distributions with algorithm \ref{algo:exactsampler}, and for each bin we counted the number of points falling in, $ E_1, \dots, E_{1000} $.
For our approximate distributions, we do the same and get counts $ O_1, \dots, O_{1000} $.
The measure of goodness for a selected coordinate is then:
\[
\chi^2 = \sum_{i=1}^{1000} \frac{(O_i - E_i)^2}{E_i}
\]






\bibliography{bibtex/polysamp_beta}
\bibliographystyle{plain}

\end{document}
